import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_restCommunication.dart';


/*

2020.9.16

Api test 디버깅용
사용하지 않음

 */

class mp_debugApi extends StatefulWidget{
  @override
  mp_debugApiState createState () => mp_debugApiState();
}
class mp_debugApiState extends State<mp_debugApi> {

  List<REST_TYPE> m_RestList = [REST_TYPE.POST_M_LOG_IN, REST_TYPE.GET_M_MYINFO, REST_TYPE.GET_M_HOME];


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(body: Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height, child: Stack( children: <Widget>[
      Positioned(top: 0, child: Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height * 0.7, child:
      ListView.separated(
          separatorBuilder: (_,__){ return SizedBox(height: 5,);},
          itemCount: m_RestList.length,
          itemBuilder: (_,_i){
            return GestureDetector(onTap: (){ClickRestApi(m_RestList[_i]);}, child: Container(width: MediaQuery.of(context).size.width, height: 150, color: Colors.primaries[Random().nextInt(Colors.primaries.length)], child: Center(child: Text(m_RestList[_i].toString(),)),),);
          }),),),
      Positioned(top: MediaQuery.of(context).size.height * 0.7, child: Text('111'),)
    ],),),);
  }

  void ClickRestApi (REST_TYPE _type){
    switch (_type){
      case REST_TYPE.POST_M_LOG_IN:
        {
          mp_restCommunication.RequestLogin(_type, ['lizzy03@naver.com','0000'], null, null, context);
        }
        break;
      case REST_TYPE.GET_M_MYINFO:
        {
          mp_restCommunication.RequestGet(_type, null, null, null, context);
        }
        break;
      case REST_TYPE.GET_M_HOME:
        {
          mp_restCommunication.RequestGet(_type, [mp_globalData.LATITUDE.toString(), mp_globalData.LONGITUDE.toString()], null, null, context);
        }
        break;
    }
  }
}