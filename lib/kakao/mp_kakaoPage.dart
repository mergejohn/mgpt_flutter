import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kakao_flutter_sdk/all.dart';
import 'package:mergepoint/mp_safeArea.dart';
import 'dart:core';

/*

2020.9.16

카카오톡 연동 테스트 페이지
로그인 / 플친 연동 확인

*/

class mp_kakaoPage extends StatefulWidget{
  mp_kakaoPageState createState ()=> mp_kakaoPageState();
}
class mp_kakaoPageState extends State<mp_kakaoPage>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      KakaoContext.clientId = '146e0c28203c2de4b6bad0563af5b4c4';
      KakaoContext.javascriptClientId = 'f32884b1950ffdfd97d602e859724a76';
    });
  }

  @override
  Widget build(BuildContext context) {
    kakaoInstalledCheck();
    // TODO: implement build
    return mp_safeArea(
      childWidget: Scaffold (
          body: Column (children: <Widget>[
            GestureDetector(onTap: (){loginWithKakao();}
              , child: Container(width: 200, height: 100, color: Colors.blue,
                child: Text('login direct'),),),

            GestureDetector(onTap: (){
              if (_isKakaotalkInstalled){ loginWithTalk(); }}
              , child: Container(width: 200, height: 100, color: Colors.yellow,
                child: _isKakaotalkInstalled ? Text('Kakao Exist') : Text('Kakao Not Exist'),),),

            GestureDetector(onTap: (){ logout();}
              , child: Container(width: 200, height: 100, color: Colors.blue,
                child: Text('logout'),),),

            GestureDetector(onTap: (){ unlink();}
              , child: Container(width: 200, height: 100, color: Colors.green,
                child: Text('unlink'),),),

            GestureDetector(onTap: (){ currentUsertoken();}
              , child: Container(width: 200, height: 100, color: Colors.grey,
                child: Text('showtokeninfo'),),),

            GestureDetector(onTap: (){ channel();}
              , child: Container(width: 200, height: 100, color: Colors.green,
                child: Text('channel'),),),
          ],)
      ),
    );
  }


  //

  bool _isKakaotalkInstalled = false;
  kakaoInstalledCheck () async {
    final installed = await isKakaoTalkInstalled();
    setState(() {
      _isKakaotalkInstalled = installed;
    });
  }

  loginWithKakao () async {
    try{
      print('loginWithKakao : try');
      var code = await AuthCodeClient.instance.request();
      await storeToken(code);
    }catch(e){
      print(e.toString());
    }
  }

  loginWithTalk () async {
    try {
      var code = await AuthCodeClient.instance.requestWithTalk();
      await storeToken(code);
    }catch (e){
      print (e.toString());
    }
  }

  storeToken (String authCode) async {
    try{
      print('storeToken : try');
      var token = await AuthApi.instance.issueAccessToken(authCode);
      print('storeToken : try1');
      AccessTokenStore.instance.toStore(token);
      print('token saved : ' + token.accessToken);
    }catch (e){
      print('storeToken : try2');
      print(e.toString());
    }
  }

  //로그아웃은 API 요청의 성공 여부와 관계 없이 사용자 토큰을 삭제 처리한다는 점에 유의합니다.
  logout () async {
    try {
      var code = await UserApi.instance.logout();
      print (code.toString());
    }catch (e){
      print (e.toString());
    }
  }

  //연결이 끊어지면 기존의 사용자 토큰은 더 이상 사용할 수 없으므로, 연결 끊기 API 요청 성공 시 로그아웃 처리가 함께 이뤄져 사용자 토큰이 삭제됩니다.
  unlink () async {
    try {
      var code = await UserApi.instance.unlink();
      print (code.toString());
    }catch (e){
      print (e.toString());
    }
  }

  //다음은 현재 캐시에 저장하여 사용 중인 사용자 토큰 정보를 출력하는 예제입니다.
  currentUsertoken () async {
    try{
      AccessTokenInfo code = await UserApi.instance.accessTokenInfo();
      print (code.id);
    }catch (e){
      print (e.toString());
    }
  }

  //
  channel () async {
    try{
      Uri _uri = await TalkApi.instance.channelChatUrl('_xexbxcxexl');
      launchBrowserTab(_uri);
    }catch (e){
      print(e.toString());
    }
  }





  Future<User> tokenRequest () async {
    try {
      User user = await UserApi.instance.me();
    } on KakaoAuthException catch (e) {
     if (e.hashCode == ApiErrorCause.INVALID_TOKEN as int){
       print(e.toString());
     }
    }
    catch (e){
      print(e.toString());
    }
  }
}