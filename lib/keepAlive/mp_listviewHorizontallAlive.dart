import 'package:flutter/cupertino.dart';

class mp_listviewHorizontallAlive extends StatefulWidget {
  mp_listviewHorizontallAlive(this.listCount, this.listItemFunction, this.separatorWidth);
  int listCount;
  double separatorWidth;
  Function(int) listItemFunction;

  mp_listviewHorizontallAliveState _aliveState = mp_listviewHorizontallAliveState();

  @override
  mp_listviewHorizontallAliveState createState ()=> _aliveState;
}
class mp_listviewHorizontallAliveState extends State<mp_listviewHorizontallAlive> with AutomaticKeepAliveClientMixin<mp_listviewHorizontallAlive>{

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.separated(
        separatorBuilder: (_,__) => SizedBox(width: widget.separatorWidth,),
        scrollDirection: Axis.horizontal,
        itemCount: widget.listCount,
        itemBuilder: (_, i) {
          return widget.listItemFunction(i);
        });
  }
}