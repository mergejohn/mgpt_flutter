import 'dart:async';

import 'package:flutter/material.dart';
import 'package:location/location.dart';

/*

2020.9.16

현재위치를 실시간으로 받아올때 사용
위치 갱신할때 사용하면 유용할듯

현재 적용은 되어있지 않음

 */

class mp_locationChaser {

  mp_locationChaser(this.locationCallBack);
  Function(LocationData) locationCallBack;

  final Location location = Location();

  StreamSubscription<LocationData> _locationSubscription;

  void StartChase (){_listenLocation();}
  void StopChase (){_stopListen();}

  Future<void> _listenLocation() async {
    _locationSubscription =
        location.onLocationChanged.handleError((dynamic err) {
          _locationSubscription.cancel();
        }).listen((LocationData currentLocation) {
          locationCallBack(currentLocation);
        });
  }

  Future<void> _stopListen() async {
    _locationSubscription.cancel();
  }
}