import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mergepoint/debug/mp_debugApi.dart';
import 'package:mergepoint/pages/mp_splashPage.dart';
import 'dart:io';

import 'package:mergepoint/pages/mp_startPage.dart';
import 'package:mergepoint/pages/mp_tutorialPage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      debugShowCheckedModeBanner: false,
      home: mp_startPage(),
//      home: mp_debugApi(),
//      home: mp_tutorialPage(),
    );

//      mp_splashPage();
  }
}