import 'package:flutter/cupertino.dart';

class mp_marqueeScroll extends StatefulWidget {
  final Widget child;
  final Axis direction;
  final bool needCenter;
  final Duration animationDuration, backDuration, pauseDuration;

  mp_marqueeScroll({
    @required this.child,
    this.direction: Axis.horizontal,
    this.needCenter: false,
    this.animationDuration: const Duration(milliseconds: 5000),
    this.backDuration: const Duration(milliseconds: 10),
    this.pauseDuration: const Duration(milliseconds: 800),
  });

  @override
  mp_marqueeScrollState createState() => mp_marqueeScrollState();
}

class mp_marqueeScrollState extends State<mp_marqueeScroll> {
  ScrollController scrollController;
  marqueeWidgetNotifier _notifier = new marqueeWidgetNotifier(Axis.horizontal);

  int _duration = 0;

  @override
  void initState() {
    scrollController = ScrollController(initialScrollOffset: 50.0);
    WidgetsBinding.instance.addPostFrameCallback(scroll);
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
//      print(scrollController.position.pixels.toString() + ' / ' + scrollController.position.maxScrollExtent.toString());
      if (scrollController.position.maxScrollExtent == 0 && widget.needCenter == true){
        _notifier.change(Axis.vertical);
      }

      double _length = scrollController.position.maxScrollExtent / context.size.width;
      print('!!!!!!!!!!!!!!1 ' + _length.toString());
      _length = _length * widget.animationDuration.inMilliseconds.toDouble();
      _duration = _length.toInt();
      print('!!!!!!!!!!!!!!3 ' + _duration.toString());

      if (_duration < widget.animationDuration.inMilliseconds){_duration = widget.animationDuration.inMilliseconds;}
    });
  }

  @override
  void dispose(){
    scrollController.dispose();
    _notifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(valueListenable: _notifier, builder: (_,_v,__) =>
        SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: _v == Axis.horizontal ? widget.child : Center(child: widget.child,),
          scrollDirection: _v,
          controller: scrollController,
        ),);
  }

  void scroll(_) async {
    while (scrollController.hasClients) {
      await Future.delayed(widget.pauseDuration);
      if(scrollController.hasClients)
        await scrollController.animateTo(
            scrollController.position.maxScrollExtent,
            duration: Duration(milliseconds: _duration),
            curve: Curves.linear);
      await Future.delayed(widget.pauseDuration);
      if(scrollController.hasClients)
        await scrollController.animateTo(0.0,
            duration: widget.backDuration, curve: Curves.linear);
    }
  }
}

class marqueeWidgetNotifier extends ValueNotifier<Axis>{
  Axis value = Axis.horizontal;
  marqueeWidgetNotifier(Axis _v):super(_v);
  void change (Axis _v){
    print('change!!!');
    value = _v;
    notifyListeners();
  }
}