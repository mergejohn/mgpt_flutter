import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:mergepoint/popup/mp_oneButtonPopup.dart';
import 'package:mergepoint/popup/mp_twoButtonPopup.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'mp_language.dart';

import 'package:flutter/material.dart';
import 'mp_restCommunication.dart';
import 'package:flutter/services.dart' show rootBundle;

const Color mainColor = Color(0xfff92d49);

//유저기본정보 저장용
//사용중이 아님 SharedPreferences 로 대체될것 같음
class mp_BasicUserData
{
  String id;
  String pass;

  bool mobileDataUse;
  bool eventReceive;
  String language;

  mp_BasicUserData(this.id, this.pass, this.mobileDataUse, this.eventReceive, this.language);

  factory mp_BasicUserData.fromJson(dynamic json) {
    return mp_BasicUserData (json['id'] as String, json['pass'] as String,
        json['mobileDataUse'] as bool, json['eventReceive'] as bool, json['language'] as String);
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'pass':pass,
    'mobileDataUse':mobileDataUse,
    'eventReceive':eventReceive,
    'language':language,
  };
}

class mp_globalData {
  static LANGUAGE_TYPE Language = LANGUAGE_TYPE.KOR;
//  static String Url = 'https://member.mergepoint.co.kr/';
  static String Url = 'https://memberdev.mergepoint.co.kr/';
  static String DebugUrl = 'https://memberdev.mergepoint.co.kr/';
  static String DevicePlatform = 'AOS';
  static String Version = '';

  static String Cookie = '';

  static String get BASE_URL => kDebugMode ? DebugUrl : Url;

  //기본 경위도
  static final double BASE_LATITUDE = 37.538778;
  static final double BASE_LONGITUDE = 126.89520;

  //현재 경위도
  static double LATITUDE = 37.538778;
  static double LONGITUDE = 126.89520;

  static SharedPreferences sharedPreferences;

  static Size MediaQuerySize;
  static double TextScaleFactor = 1;
  static double TopPadding = 0;
  static double BottomPadding = 0;
  static double BottomNavigationHeight = 60;

  static mp_BasicUserData UserData;
  static Result_User_class LoginData;

  static Future<File> writeFile (String _data, String _fname) async {
    final file = await getApplicationDocumentsDirectory();
    return File(file.path + '/' + _fname).writeAsString(_data);
  }
  static Future<String> readFile (String _fname) async {
    try {
      final dir = await getApplicationDocumentsDirectory();
      return await File(dir.path + '/' + _fname).readAsString();
    }catch (e){
      return null;
    }
  }
  static Future<bool> deleteFile (String _fname) async {
    try {
      File _file;
      final dir = await getApplicationDocumentsDirectory();
      _file  = await File(dir.path + '/' + _fname);
      _file.delete();
      return true;
    }catch (e){
      return false;
    }
  }

  static void setScreenSize (MediaQueryData _media){
    TextScaleFactor = _media.textScaleFactor;
    TopPadding = _media.padding.top;
    BottomPadding = _media.padding.bottom;
    MediaQuerySize = Size(_media.size.width, _media.size.height);
    print('--------------------------------------');
    print(MediaQuerySize.height.toString() + ' / ' + TopPadding.toString() + ' / ' + BottomPadding.toString());
  }

  static String getDateString (String _dtm, bool _includeTime)
  {
    var date = new DateTime.fromMillisecondsSinceEpoch(int.parse(_dtm));
    if (_includeTime)
    {
      DateFormat _format = DateFormat("yyyy.MM.dd hh:mm");
      return _format.format(date);
    }
    DateFormat _format = DateFormat("yyyy.MM.dd");
    return _format.format(date);
  }

  static String getTimeString (int _rt){

    var _d = Duration(milliseconds: _rt);
    String _h = _d.inHours.toString();
    if (_h.length == 1){
      _h = "0"+_h.toString();
    }
    String _m = _d.inMinutes.toString();
    if (_m.length == 1){
      _m = "0"+_m.toString();
    }
    String _s = _d.inSeconds.remainder(60).toString();
    if (_s.length == 1){
      _s = "0"+_s.toString();
    }
    return _h + ":" + _m + ":" + _s;
  }

  static void showErrorPopup (BuildContext _c, String _msg, Function _error){
    showDialog(context: _c, builder: (_) => mp_oneButtonSingleLinePopup(
      title: mp_restCommunication.IsStatusOkByString(_msg),
      btText: mp_language.Language(mp_globalData.Language, TEXT_TYPE.OK),
      popupHeight: 300,
      btClick:(){Navigator.pop(_c, true); if (_error != null){_error();}},)
    );
  }

  static void showScrollPopup (BuildContext _c, double _height, String _msg){
    showDialog(context: _c, builder: (_) => mp_oneButtonScrollPopup(
      title: _msg,
      btText: mp_language.Language(mp_globalData.Language, TEXT_TYPE.OK),
      popupHeight: _height,
      btClick:(){Navigator.pop(_c, true);},)
    );
  }

  static void showNoramlPopup (BuildContext _c, double _height, String _msg){
    showDialog(context: _c, builder: (_) => mp_oneButtonSingleLinePopup(
      title: _msg,
      btText: mp_language.Language(mp_globalData.Language, TEXT_TYPE.OK),
      popupHeight: _height,
      btClick:(){Navigator.pop(_c, true);},)
    );
  }

  static void showOneBtAlarmPopupWithFunc (BuildContext _c, double _height, String _msg, Function _callback){
    showDialog(context: _c, builder: (_) => mp_oneButtonAlramPopup(
      title: _msg,
      btText: mp_language.Language(mp_globalData.Language, TEXT_TYPE.OK),
      popupHeight: _height,
      btClick:(){ _callback(); Navigator.pop(_c, true);},)
    );
  }

  static void showTwoBtPopup (BuildContext _c, double _height, String _title, String _msg, String _leftBtString, String _rightBtString, Function _callback){
    showDialog(context: _c, builder: (_) => mp_twoButtonPopup(
      title: _title,
      desc: _msg,
      popupHeight: _height,
      leftBtText: _leftBtString,
      leftClick: (){ Navigator.pop(_c, true);},
      rightBtText: _rightBtString,
      rightClick:(){ _callback(); Navigator.pop(_c, true);},)
    );
  }

  static BuildContext loadingContext;
  static void showLoading (BuildContext _c) {
    loadingContext = _c;
    showDialog(barrierDismissible: false, context: _c, builder: (_) => Container(width: 50, height: 50, child: Center(child: CircularProgressIndicator(),)));
  }
  static void hideLoading (){
    if (loadingContext != null){
      Navigator.of(loadingContext, rootNavigator: true).pop(false);
    }
  }

  static String countCheck (int _count){
    if ( _count > 999999)
    {
      _count = 999999;
    }

    if (_count >= 1000)
    {
      double _valueK = _count / 1000;
      double _valueH = (_count / 100) - (_valueK * 10);

      int _k = _valueK.toInt();
      int _h = _valueH.toInt();

      return _k.toString() + "." + _h.toString() + "K";
    }
    return _count.toString();
  }
}

class mp_PageOptions {
//  static loginPageBackButtonShowNotifier IsLoginPageBackButtonShow;
  //메인화면으로 갈때는 로그인 페이지가 사라진 상황이기 때문에 초기화시 플레그값만 전달해주면 된다.
  static bool IsLoginPageBackBtShow = true;
}

enum REGULAR_WRONG {
  NONE,
  SPECIAL,
  ENGLISH,
  NUMBER,
  KOREA,
}

class RegularExpression {
  REGULAR_WRONG checkPossiblePasswordText(String value) {
    final  validNumbers = RegExp(r'(\d+)');
    final  validAlphabet = RegExp(r'[a-zA-Z]');
    final  validSpecial = RegExp(r'[~!@#$%^&*]');
    final  validKorea = RegExp(r'[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]');

    //한글이있는지 확인
    if(validKorea.hasMatch(value)) {
      print("reqular : korea");
      return REGULAR_WRONG.KOREA;
    }

    //특수기호가 있는지 확인
    if(!validSpecial.hasMatch(value)) {
      print("reqular : special");
      return REGULAR_WRONG.SPECIAL;
    }

    //문자가 있는지 확인
    if(!validAlphabet.hasMatch(value)) {
      print("reqular : english");
      return REGULAR_WRONG.ENGLISH;
    }

    //숫자가 있는지 확인
    if(!validNumbers.hasMatch(value)) {
      print("reqular : number");
      return REGULAR_WRONG.NUMBER;
    }
    return REGULAR_WRONG.NONE;
  }
}

class mp_filter {
  static List<String> _words;
  static void Load (){
    loadAsset('filter/filter.txt').then((value){
      _words = value.split(',');
    });
  }
  static Future<String> loadAsset(String fn) async {
    return await rootBundle.loadString(fn);
  }
  static bool checkWord (String _msg){
    print('checkword : ' + _msg);

    var _word = _msg.split(' ');
    for (int i = 0 ; i < _word.length ; i++){
      if (_words.contains(_word[i])){
        print(_word[i]);
        return true;
      }
    }
    print('clean word');
    return false;
  }
}

class mp_ResultDataAdd <T> {
  mp_ResultDataAdd(List<T> _result, List<T> _to){
    List<T> _resultList = _result;
    if (_resultList != null){
      for (int i = 0 ; i < _resultList.length ; i++){
        _to.add(_resultList[i]);
      }
    }
  }
}

class mp_StreamTimeChecker {
  int _maxMillisec = 0;
  int _startTime = 0;
  mp_StreamTimeChecker(this._maxMillisec);
  void Start (){
    _startTime = new DateTime.now().millisecondsSinceEpoch;

    print('GetTerm Start : ' + _startTime.toString());

  }
  int GetTerm(){

    int _term = DateTime.now().millisecondsSinceEpoch;
    _term -=  _startTime;
    print('GetTerm _term : ' + _term.toString());
    if (_term >= _maxMillisec){
      return 0;
    }

    print('GetTerm max : ' + _maxMillisec.toString());
    print('GetTerm term : ' + _term.toString());
    print('GetTerm remain : ' + (_maxMillisec - _term).toString());

    return _maxMillisec - _term;
  }
}