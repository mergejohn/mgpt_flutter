
/*

과거 사용했던 더미
혹시 몰라서 지우지 않고 있다.
다국어 사용시 사용할듯

 */

enum LANGUAGE_TYPE {
  KOR,
  ENG,
  JP
}

enum TEXT_TYPE
{
  //IOS권한 셋팅해주세요
  IOS_PERMISSION_SETTING_DESC,



  //마이페이지
  PLZ_LOGIN,      //로그인하세요
  LOGIN,        //로그인
  LOGOUT,     //로그아웃

  //-> 작업필요

  //login
  SIGN,       //회원가입
  ID,             //아이디
  PASS,           //비밀번호
  FINDID,         //아이디찾
  FINDPASS,       //비밀번호찾기

  //find
  FIND,   //찾
  FINDPAGE_TITLE,
  FINDPAGE_DESC_ONE,
  FINDPAGE_DESC_TWO_ID,
  FINDPAGE_DESC_TWO_PASS,
  NAME,
  EMAILINPUT,
  OK,

  PLZ_INPUT_NAME,
  NAME_IS_TOO_LONG,
  PLZ_INPUT_EMAIL,
  EMAIL_IS_TOO_LONG,

  PLZ_INPUT_ID,
  ID_IS_TOO_LONG,
  YOUR_ID_IS,
  YOUR_PASS_IS,
  RESULT_TAIL,    //입니다.

  PLZ_FILL_IN_FORMS,

  //signin
  PASSCONFIRM,
  NICKNAME,
  EMAIL,
  GENDER,
  BIRTH,
  COUNTRY,
  ALLAGREE,
  USEAGREE,
  PRIVATEAGREE,
  AGEAGREE,
  EVENTAGREE,
  EMAILCONFIRM,
  PLZ_INPUT_PASS,
  PLZ_INPUT_PASSCONFIRM,
  PLZ_INPUT_NICKNAME,
  MAN,
  WOMAN,
  ID_INPUT_DESC,
  PASS_INPUT_DESC,
  PASSCONFIRM_INPUT_DESC,
  PASSCONFIRM_INPUT_ERROR_DESC,
  NICK_INPUT_DESC,
  NAME_INPUT_DESC,
  EMAIL_INPUT_DESC,
  CONFIRM,    //중복확인
  ALREADY_USE,
  USEABLE,
  INFOFORANALY,       //통계로만 사용
  AGREEWITHUSE,
  VIEW_TERM,    //약관보기

  ER_ID_TOO_SHORT,
  ER_ID_TOO_LONG,
  ER_RULE,
  ER_ALREADY_USE,
  ER_USEABLE,
  ER_ID_RULE,
  ER_ID_NOT_CONFIRM,
  ER_PASS_TOO_SHORT,
  ER_PASS_RULE,
  ER_PASS_NOT_MATCH,
  ER_NAME_TOO_SHORT,
  ER_NAME_TOO_LONG,
  ER_NAME_RULE,
  ER_NICK_TOO_SHORT,
  ER_NICK_TOO_LONG,
  ER_NICK_NOT_CONFIRM,
  ER_NICK_RULE,
  ER_EMAIL_NOT_CONFIRM,
  ER_NEED_AGREE,
  ER_SELECT_GENDER,
  ER_USE_PRIVATE_POLICY_CHECK,

  //player
  PLZ_INPUT_CHAT_COMMANT,

  //mypage
  SETTING,
  MOBILEUSE,
  EVENTRECEIVE,
  LANGUAGECHNAGE,
  NOTICE,
  HELP,
  USEPOLICY,
  PRIVATEPOLICY,
  SIGNOUT,
  APPVERSION,

  //signout popup
  SIGNOUT_WARNING_ASK,
  SIGNOUT_WARNING,
  RS_LACKCONTENT,
  RS_INCONVENIENT,
  RS_NOFAVORITE_STAR,
  RS_OUTOFFUN,
  RS_EXPENSIVE,

  //popup
  CANCLE,
  BUY,
  LOGOUTASK,

  //welcome
  WELCOME,

  //ch detail
  INPUT_MESSAGE,
  INPUT_WARNING,

  //exit
  APPEXIT,

  //network error
  PLZ_TRY_AGAIN,

  //
  ER_ALEADY_SIGNED,
  ER_SIGN_FAILED,
  ER_RESIGN_IMPOSSIBLE,           //--
  ER_NOT_SIGNED_ACCOUNT,
  ER_LOGIN_FAILED,
  ER_WITHDRAWN_ACCOUNT,
  ER_NOT_ACTIVITY_ACCOUNT,        //메일인증 안된 계
  ER_ACCESS_DINYED_ACCOUNT,
  ER_DORMANCY_ACCOUNT,        //휴면계
  ER_VALIDITY_EXPIRED,
  ER_NICKNAME,
  ER_ACCOUNT,
  ER_EMAIL,

  ER_WRONG_DATA,

  ER_EXPIRED_ACCESS,
  ER_TOO_MANY_REQUEST,

  //store
  ER_CANT_BUY_MORE,
  ER_RECEIPTCHECK_FAILED,
  NO_PRODUCTS,

  //
  SWEAR,

  //live
  NOT_ONAIR,

  //chat
  REPORT_TITLE,
  REPORT_RESULT,

  //
  UNABLE_PLAY,        //영상재생 권한이 없을경우

  //ch
  MAKEFANFORMYCH,

  //
  MOBILEDATAWARNING,

  //app update
  APPUPDATE,

  //
  NOMYCARD,

  //
  PERMIT,

  //
  UNLIMITE,
}

class mp_language {
  static String Language (LANGUAGE_TYPE _country, TEXT_TYPE _type){
    switch (_type){
      case TEXT_TYPE.IOS_PERMISSION_SETTING_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "위치 정보 확인을 위해 설정에서 위치 권한을 허용해주세요.\n\niPhone의 '설정 > 개인 정보 보호 > 위치 서비스'에 위치 서비스 항목을 허용해주시고 다시 시도해주세요";
            case LANGUAGE_TYPE.ENG: return "";
            case LANGUAGE_TYPE.JP: return "";
          }
        }
      break;


      case TEXT_TYPE.PLZ_LOGIN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "로그인 하세요";
            case LANGUAGE_TYPE.ENG: return "Please log in";
            case LANGUAGE_TYPE.JP: return "ログインしてください。";
          }
        }
        break;
      case TEXT_TYPE.LOGIN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "로그인";
            case LANGUAGE_TYPE.ENG: return "Log in";
            case LANGUAGE_TYPE.JP: return "ログイン";
          }
        }
        break;
      case TEXT_TYPE.LOGOUT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "로그아웃";
            case LANGUAGE_TYPE.ENG: return "Log out";
            case LANGUAGE_TYPE.JP: return "ログアウト";
          }
        }
        break;
      case TEXT_TYPE.SIGN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "회원가입";
            case LANGUAGE_TYPE.ENG: return "Sign up";
            case LANGUAGE_TYPE.JP: return "会員登録";
          }
        }
        break;
      case TEXT_TYPE.ID:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디";
            case LANGUAGE_TYPE.ENG: return "ID";
            case LANGUAGE_TYPE.JP: return "ID";
          }
        }
        break;
      case TEXT_TYPE.PASS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호";
            case LANGUAGE_TYPE.ENG: return "Password";
            case LANGUAGE_TYPE.JP: return "パスワード";
          }
        }
        break;
      case TEXT_TYPE.FINDID:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디 찾기";
            case LANGUAGE_TYPE.ENG: return "Find ID";
            case LANGUAGE_TYPE.JP: return "ID検索";
          }
        }
        break;
      case TEXT_TYPE.FINDPASS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호 찾기";
            case LANGUAGE_TYPE.ENG: return "Find Password";
            case LANGUAGE_TYPE.JP: return "パスワード探し";
          }
        }
        break;

    //
      case TEXT_TYPE.FIND : {
        switch (_country)
        {
          case LANGUAGE_TYPE.KOR: return "찾기";
          case LANGUAGE_TYPE.ENG: return "Find";
          case LANGUAGE_TYPE.JP: return "探し";
        }
      }
      break;
      case TEXT_TYPE.FINDPAGE_TITLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "ID/PW 찾기";
            case LANGUAGE_TYPE.ENG: return "Find ID/PW";
            case LANGUAGE_TYPE.JP: return "ID/パスワード探し";
          }
        }
        break;
      case TEXT_TYPE.FINDPAGE_DESC_ONE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "회원가입 시 입력하신";
            case LANGUAGE_TYPE.ENG: return "you entered at the time of signing up.";
            case LANGUAGE_TYPE.JP: return "会員登録時に入力した";
          }
        }
        break;
      case TEXT_TYPE.FINDPAGE_DESC_TWO_ID:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름과 이메일을 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter the name and email";
            case LANGUAGE_TYPE.JP: return "名前とEメールを入力してください。";
          }
        }
        break;
      case TEXT_TYPE.FINDPAGE_DESC_TWO_PASS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디와 이메일을 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter the ID and email";
            case LANGUAGE_TYPE.JP: return "IDとEメールを入力してください。";
          }
        }
        break;
      case TEXT_TYPE.NAME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름";
            case LANGUAGE_TYPE.ENG: return "Name";
            case LANGUAGE_TYPE.JP: return "名前";
          }
        }
        break;
      case TEXT_TYPE.EMAILINPUT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일 입력";
            case LANGUAGE_TYPE.ENG: return "Email";
            case LANGUAGE_TYPE.JP: return "Eメール";
          }
        }
        break;
      case TEXT_TYPE.OK:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "확인";
            case LANGUAGE_TYPE.ENG: return "OK";
            case LANGUAGE_TYPE.JP: return "確認";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_NAME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름을 입력해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your name";
            case LANGUAGE_TYPE.JP: return "名前を入力してください。";
          }
        }
        break;
      case TEXT_TYPE.NAME_IS_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름이 너무 깁니다.";
            case LANGUAGE_TYPE.ENG: return "The name is too long.";
            case LANGUAGE_TYPE.JP: return "名前がとても長いです。";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_EMAIL:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일을 입력해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your email.";
            case LANGUAGE_TYPE.JP: return "Eメールを入力してください。";
          }
        }
        break;
      case TEXT_TYPE.EMAIL_IS_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일이 너무 깁니다.";
            case LANGUAGE_TYPE.ENG: return "Email is too long.";
            case LANGUAGE_TYPE.JP: return "Eメールが長すぎます。";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_ID:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디를 입력해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your ID.";
            case LANGUAGE_TYPE.JP: return "IDを入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ID_IS_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디가 너무 깁니다.";
            case LANGUAGE_TYPE.ENG: return "ID is too long.";
            case LANGUAGE_TYPE.JP: return "IDが長すぎます。";
          }
        }
        break;
      case TEXT_TYPE.YOUR_ID_IS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "회원님의 아이디는";
            case LANGUAGE_TYPE.ENG: return "Your ID is";
            case LANGUAGE_TYPE.JP: return "会員様のIDは";
          }
        }
        break;
      case TEXT_TYPE.YOUR_PASS_IS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "입력하신 이메일로\n비밀번호 재설정 링크를 발송했습니다.";
            case LANGUAGE_TYPE.ENG: return "The password reset link has been sent to\nthe email you entered.";
            case LANGUAGE_TYPE.JP: return "入力されたEメールでパスワード再設定\nリンクを送信しました。";
          }
        }
        break;
      case TEXT_TYPE.RESULT_TAIL:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "입니다.";
            case LANGUAGE_TYPE.ENG: return "";
            case LANGUAGE_TYPE.JP: return "";
          }
        }
        break;
      case TEXT_TYPE.PLZ_FILL_IN_FORMS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "정보를 입력해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please fill in all forms.";
            case LANGUAGE_TYPE.JP: return "抜けた情報を入力してください。";
          }
        }
        break;

    //sign up
      case TEXT_TYPE.PASSCONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호 확인";
            case LANGUAGE_TYPE.ENG: return "Confirm Password";
            case LANGUAGE_TYPE.JP: return "パスワード確認";
          }
        }
        break;
      case TEXT_TYPE.NICKNAME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임";
            case LANGUAGE_TYPE.ENG: return "Nickname";
            case LANGUAGE_TYPE.JP: return "ニックネーム";
          }
        }
        break;
      case TEXT_TYPE.EMAIL:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일";
            case LANGUAGE_TYPE.ENG: return "E-mail";
            case LANGUAGE_TYPE.JP: return "Eメール";
          }
        }
        break;
      case TEXT_TYPE.GENDER:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "성별";
            case LANGUAGE_TYPE.ENG: return "Gender";
            case LANGUAGE_TYPE.JP: return "性別";
          }
        }
        break;
      case TEXT_TYPE.BIRTH:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "출생년도";
            case LANGUAGE_TYPE.ENG: return "Year of Birth";
            case LANGUAGE_TYPE.JP: return "誕生年";
          }
        }
        break;
      case TEXT_TYPE.COUNTRY:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "국적 선택";
            case LANGUAGE_TYPE.ENG: return "Nationality";
            case LANGUAGE_TYPE.JP: return "国籍選択";
          }
        }
        break;
      case TEXT_TYPE.ALLAGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "모두 동의합니다.";
            case LANGUAGE_TYPE.ENG: return "I agree to everything.";
            case LANGUAGE_TYPE.JP: return "全部同意します。";
          }
        }
        break;
      case TEXT_TYPE.USEAGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "[필수] 이용약관 동의";
            case LANGUAGE_TYPE.ENG: return "[Essential] Terms and Conditions agreement";
            case LANGUAGE_TYPE.JP: return "[必須] 利用規約に同意";
          }
        }
        break;
      case TEXT_TYPE.PRIVATEAGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "[필수] 개인정보 처리방침 동의";
            case LANGUAGE_TYPE.ENG: return "[Essential] Privacy Policy Statement agreement";
            case LANGUAGE_TYPE.JP: return "[必須] プライバシー同意";
          }
        }
        break;
      case TEXT_TYPE.AGEAGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "[필수] 본인은 만 14세 이상입니다.";
            case LANGUAGE_TYPE.ENG: return "[Essential] I am over 14 years old.";
            case LANGUAGE_TYPE.JP: return "[必須] 私は14歳以上です。";
          }
        }
        break;
      case TEXT_TYPE.EVENTAGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "[선택] 이벤트/예약정보 SMS/Push 수신동의";
            case LANGUAGE_TYPE.ENG: return "[Optional] Accepting SMS/Push for events/reservation information.";
            case LANGUAGE_TYPE.JP: return "[選択] イベント/予約情報SMS/Push受信に同意";
          }
        }
        break;
      case TEXT_TYPE.EMAILCONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일 인증";
            case LANGUAGE_TYPE.ENG: return "Email authentication";
            case LANGUAGE_TYPE.JP: return "Eメール認証";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_PASS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호 입력";
            case LANGUAGE_TYPE.ENG: return "Enter Password";
            case LANGUAGE_TYPE.JP: return "パスワード入力";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_PASSCONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호 확인";
            case LANGUAGE_TYPE.ENG: return "Confirm Password";
            case LANGUAGE_TYPE.JP: return "パスワード確認";
          }
        }
        break;
      case TEXT_TYPE.PLZ_INPUT_NICKNAME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임을 입력해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Enter Nickname";
            case LANGUAGE_TYPE.JP: return "ニックネームを入力してください。";
          }
        }
        break;
      case TEXT_TYPE.MAN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "남성";
            case LANGUAGE_TYPE.ENG: return "MAN";
            case LANGUAGE_TYPE.JP: return "男";
          }
        }
        break;
      case TEXT_TYPE.WOMAN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "여성";
            case LANGUAGE_TYPE.ENG: return "WOMAN";
            case LANGUAGE_TYPE.JP: return "女";
          }
        }
        break;
      case TEXT_TYPE.ID_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "영문, 숫자로 20자까지 가능";
            case LANGUAGE_TYPE.ENG: return "Up to 20 characters in English and numerals";
            case LANGUAGE_TYPE.JP: return "英語、数字で20文字まで";
          }
        }
        break;
      case TEXT_TYPE.PASS_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return r"영문,숫자,기호를 조합한 8자 이상 (~ ! @ # $ % ^ & *)";
            case LANGUAGE_TYPE.ENG: return r"8 or more characters combined with English, Numeric, and Symbols. (~ ! @ # $ % ^ & *)";
            case LANGUAGE_TYPE.JP: return r"英数字、記号を組み合わせた8文字以上 (~ ! @ # $ % ^ & *)";
          }
        }
        break;
      case TEXT_TYPE.PASSCONFIRM_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호가 일치합니다.";
            case LANGUAGE_TYPE.ENG: return "Password matches";
            case LANGUAGE_TYPE.JP: return "パスワードが一致しています。";
          }
        }
        break;
      case TEXT_TYPE.PASSCONFIRM_INPUT_ERROR_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호가 일치하지 않습니다.";
            case LANGUAGE_TYPE.ENG: return "Passwords do not match.";
            case LANGUAGE_TYPE.JP: return "パスワードが一致しません。";
          }
        }
        break;
      case TEXT_TYPE.NICK_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "App에서 사용할 닉네임으로 추후 변경 불가하므로 신중히 선택해 주세요. 한글,영문,숫자로 20자까지 가능";
            case LANGUAGE_TYPE.ENG: return "This is a nickname to be used in the app, and you can't change it later, so please select it carefully. (Up to 20 characters.)";
            case LANGUAGE_TYPE.JP: return "Appで使用するニックネームで、後日変更できませんので慎重に選択してください。ハングル、英語、数字で20文字まで";
          }
        }
        break;
      case TEXT_TYPE.NAME_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "한글,영문,숫자로 20자까지 가능";
            case LANGUAGE_TYPE.ENG: return "Up to 20 characters";
            case LANGUAGE_TYPE.JP: return "ハングル、英語、数字で20文字まで";
          }
        }
        break;
      case TEXT_TYPE.EMAIL_INPUT_DESC:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일은 비밀번호 분실 시 필요하므로 반드시 정확한 주소를 입력해 주세요. 가입완료 후 메일인증을 거쳐야 서비스를 이용하실 수 있습니다.";
            case LANGUAGE_TYPE.ENG: return "Please make sure to enter the correct address for the email as it will be necessary if you lose your password. You must authenticate your email after signing up for the service.";
            case LANGUAGE_TYPE.JP: return "Eメールは、パスワードを紛失した場合に必要となりますので、必ず正確な住所を入力してください。 登録完了後、Eメール認証が必要となります。";
          }
        }
        break;
      case TEXT_TYPE.CONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "중복확인";
            case LANGUAGE_TYPE.ENG: return "Check";
            case LANGUAGE_TYPE.JP: return "重複チェック";
          }
        }
        break;
      case TEXT_TYPE.ALREADY_USE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이미 사용중 입니다.";
            case LANGUAGE_TYPE.ENG: return "Already occupied. please choose another.";
            case LANGUAGE_TYPE.JP: return "すでに使用中です。";
          }
        }
        break;
      case TEXT_TYPE.USEABLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "사용가능 합니다.";
            case LANGUAGE_TYPE.ENG: return "Available.";
            case LANGUAGE_TYPE.JP: return "使用可能です。";
          }
        }
        break;
      case TEXT_TYPE.INFOFORANALY:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아래의 선택 항목은 유저 통계를 위해서만 사용됩니다.";
            case LANGUAGE_TYPE.ENG: return "The data below is used only for user statistics.";
            case LANGUAGE_TYPE.JP: return "以下の選択項目はユーザーの統計のためだけに使用されます。";
          }
        }
        break;
      case TEXT_TYPE.AGREEWITHUSE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아래 최종 사용자 라이선스 동의를 하셔야 이용이 가능합니다.";
            case LANGUAGE_TYPE.ENG: return "You must agree to the end user license agreement below to use this app.";
            case LANGUAGE_TYPE.JP: return "";
          }
        }
        break;

      case TEXT_TYPE.VIEW_TERM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "약관보기";
            case LANGUAGE_TYPE.ENG: return "View Terms";
            case LANGUAGE_TYPE.JP: return "利用規約を読む";
          }
        }
        break;

      case TEXT_TYPE.ER_ID_TOO_SHORT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디를 2글자 이상 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter an ID of at least 2 characters.";
            case LANGUAGE_TYPE.JP: return "IDを2文字以上入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_ID_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디를 20자 이내로 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your ID within 20 characters.";
            case LANGUAGE_TYPE.JP: return "IDを20文字以内で入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_RULE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "규칙에 맞게 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter according to the rules";
            case LANGUAGE_TYPE.JP: return "規則に合わせて入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_ALREADY_USE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이미 사용 중입니다.";
            case LANGUAGE_TYPE.ENG: return "Already occupied";
            case LANGUAGE_TYPE.JP: return "すでに使用中です。";
          }
        }
        break;
      case TEXT_TYPE.ER_USEABLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "사용 가능합니다.";
            case LANGUAGE_TYPE.ENG: return "available for use";
            case LANGUAGE_TYPE.JP: return "使用可能です。";
          }
        }
        break;
      case TEXT_TYPE.ER_ID_RULE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디에 잘못된 문자가 포함되어있습니다.";
            case LANGUAGE_TYPE.ENG: return "The ID contains invalid characters.";
            case LANGUAGE_TYPE.JP: return "IDに誤った文字が含まれています。";
          }
        }
        break;
      case TEXT_TYPE.ER_ID_NOT_CONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "아이디 중복체크 해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please check your ID.";
            case LANGUAGE_TYPE.JP: return "IDの重複チェックをしてください。";
          }
        }
        break;
      case TEXT_TYPE.ER_PASS_TOO_SHORT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호를 8글자 이상 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter at least 8 characters.";
            case LANGUAGE_TYPE.JP: return "パスワードを8文字以上入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_PASS_RULE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호를 규칙에 맞게\n입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your password according to the rules.";
            case LANGUAGE_TYPE.JP: return "パスワードを規則に合わせて入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_PASS_NOT_MATCH:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호가 일치하지 않습니다.";
            case LANGUAGE_TYPE.ENG: return "Passwords do not match.";
            case LANGUAGE_TYPE.JP: return "パスワードが一致しません。";
          }
        }
        break;
      case TEXT_TYPE.ER_NAME_TOO_SHORT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름을 2글자 이상 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your name at least 2 characters";
            case LANGUAGE_TYPE.JP: return "名前を2文字以上入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NAME_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름을 20자 이내로 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your name within 20 characters";
            case LANGUAGE_TYPE.JP: return "名前を20文字以内で入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NAME_RULE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이름을 규칙에 맞게 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your name according to the rules";
            case LANGUAGE_TYPE.JP: return "名前を規則に合わせて入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NICK_TOO_SHORT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임을 2글자 이상 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter a nickname of at least 2 characters.";
            case LANGUAGE_TYPE.JP: return "ニックネームを2文字以上入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NICK_TOO_LONG:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임을 20자 이내로 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your nickname within 20 characters.";
            case LANGUAGE_TYPE.JP: return "ニックネームを2文字以上入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NICK_NOT_CONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임 중복체크를 해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please check nickname duplication.";
            case LANGUAGE_TYPE.JP: return "ニックネームの重複チェックをしてください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NICK_RULE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "닉네임을 규칙에 맞게 입력해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please enter your nickname according to the rules.";
            case LANGUAGE_TYPE.JP: return "ニックネームを規則正しく入力してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_EMAIL_NOT_CONFIRM:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이메일 중복체크를 해주세요.";
            case LANGUAGE_TYPE.ENG: return "Please check email duplication.";
            case LANGUAGE_TYPE.JP: return "Eメールの重複チェックをしてください。";
          }
        }
        break;
      case TEXT_TYPE.ER_NEED_AGREE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "필수항목 동의에 체크해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please check the consent of the required terms.";
            case LANGUAGE_TYPE.JP: return "必須項目の同意をチェックしてください。";
          }
        }
        break;
      case TEXT_TYPE.ER_SELECT_GENDER:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "성별을 선택해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please select your gender.";
            case LANGUAGE_TYPE.JP: return "性別を選択してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_USE_PRIVATE_POLICY_CHECK:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이용약관 및 개인정보 처리방침에 동의해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please agree to the required terms and privacy policy.";
            case LANGUAGE_TYPE.JP: return "利用規約及び個人情報の処理方針に同意してください。";
          }
        }
        break;

    //
      case TEXT_TYPE.PLZ_INPUT_CHAT_COMMANT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "채팅을 입력하세요. (최대 68자)";
            case LANGUAGE_TYPE.ENG: return "Chat with friends. (Up to 124 characters)";
            case LANGUAGE_TYPE.JP: return "話を入力してください。 (最大68字)";
          }
        }
        break;

    //
      case TEXT_TYPE.SETTING:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "환경설정";
            case LANGUAGE_TYPE.ENG: return "Setting";
            case LANGUAGE_TYPE.JP: return "環境設定";
          }
        }
        break;
      case TEXT_TYPE.MOBILEUSE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "모바일 데이터 사용";
            case LANGUAGE_TYPE.ENG: return "Enable celluar data";
            case LANGUAGE_TYPE.JP: return "モバイルデータ使用";
          }
        }
        break;
      case TEXT_TYPE.EVENTRECEIVE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이벤트 정보 수신 동의";
            case LANGUAGE_TYPE.ENG: return "Agree to receive event messages";
            case LANGUAGE_TYPE.JP: return "イベント情報受信同意";
          }
        }
        break;
      case TEXT_TYPE.LANGUAGECHNAGE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "언어변경";
            case LANGUAGE_TYPE.ENG: return "Change Language";
            case LANGUAGE_TYPE.JP: return "言語を変更";
          }
        }
        break;
      case TEXT_TYPE.NOTICE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "공지사항";
            case LANGUAGE_TYPE.ENG: return "Notice";
            case LANGUAGE_TYPE.JP: return "お知らせ";
          }
        }
        break;
      case TEXT_TYPE.HELP:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "도움말";
            case LANGUAGE_TYPE.ENG: return "Help";
            case LANGUAGE_TYPE.JP: return "ヘルプ";
          }
        }
        break;
      case TEXT_TYPE.USEPOLICY:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이용약관";
            case LANGUAGE_TYPE.ENG: return "Terms and Conditions";
            case LANGUAGE_TYPE.JP: return "利用規約";
          }
        }
        break;
      case TEXT_TYPE.PRIVATEPOLICY:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "개인정보처리방침";
            case LANGUAGE_TYPE.ENG: return "Privacy Policy Statement";
            case LANGUAGE_TYPE.JP: return "個人情報処理方針";
          }
        }
        break;
      case TEXT_TYPE.SIGNOUT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "탈퇴";
            case LANGUAGE_TYPE.ENG: return "Withdrawal";
            case LANGUAGE_TYPE.JP: return "脱退";
          }
        }
        break;
      case TEXT_TYPE.APPVERSION:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "앱 버전";
            case LANGUAGE_TYPE.ENG: return "version";
            case LANGUAGE_TYPE.JP: return "アプリバージョン";
          }
        }
        break;

    //
      case TEXT_TYPE.SIGNOUT_WARNING_ASK:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "정말로 탈퇴하시겠습니까?";
            case LANGUAGE_TYPE.ENG: return "Do you want to leave?";
            case LANGUAGE_TYPE.JP: return "本当に脱退しますか?";
          }
        }
        break;
      case TEXT_TYPE.SIGNOUT_WARNING:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "탈퇴 시 구매내역과 회원 정보가 모두 삭제되며, 동일한 아이디/이메일/닉네임을 재사용할 수 없습니다.";
            case LANGUAGE_TYPE.ENG: return "Upon withdrawal, both purchase details and member information will be deleted, and the same ID/email/nickname cannot be reused.";
            case LANGUAGE_TYPE.JP: return "本当に退会しますか? 退会時、購入履歴と会員情報がすべて削除され、同一のID/Eメール/ニックネームは再使用できません。";
          }
        }
        break;
      case TEXT_TYPE.RS_LACKCONTENT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "컨텐츠부족";
            case LANGUAGE_TYPE.ENG: return "Lack of content";
            case LANGUAGE_TYPE.JP: return "コンテンツ不足";
          }
        }
        break;
      case TEXT_TYPE.RS_INCONVENIENT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "App 사용불편";
            case LANGUAGE_TYPE.ENG: return "App Inconvenience";
            case LANGUAGE_TYPE.JP: return "アプリの使い勝手が悪い";
          }
        }
        break;
      case TEXT_TYPE.RS_NOFAVORITE_STAR:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "마음에 드는 스타가 없어서";
            case LANGUAGE_TYPE.ENG: return "No favorite celebrities";
            case LANGUAGE_TYPE.JP: return "気に入るスターがいないので";
          }
        }
        break;
      case TEXT_TYPE.RS_OUTOFFUN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "재미가 없어서";
            case LANGUAGE_TYPE.ENG: return "Out of fun";
            case LANGUAGE_TYPE.JP: return "面白くなくて";
          }
        }
        break;
      case TEXT_TYPE.RS_EXPENSIVE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "상품이 비싸서";
            case LANGUAGE_TYPE.ENG: return "The items are expensive";
            case LANGUAGE_TYPE.JP: return "商品が高くて";
          }
        }
        break;

    //
      case TEXT_TYPE.CANCLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "취소";
            case LANGUAGE_TYPE.ENG: return "Cancel";
            case LANGUAGE_TYPE.JP: return "キャンセル";
          }
        }
        break;
      case TEXT_TYPE.BUY:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "구매";
            case LANGUAGE_TYPE.ENG: return "Buy";
            case LANGUAGE_TYPE.JP: return "購入";
          }
        }
        break;
      case TEXT_TYPE.LOGOUTASK:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "정말 로그아웃 하시겠습니까?";
            case LANGUAGE_TYPE.ENG: return "Do you want to log out?";
            case LANGUAGE_TYPE.JP: return "本当にログアウトしますか？";
          }
        }
        break;

    //
      case TEXT_TYPE.WELCOME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "입력하신 메일주소로\n발송된 인증메일을 확인해\n주셔야 서비스 이용이\n가능합니다.";
            case LANGUAGE_TYPE.ENG: return "You must check the authentication mail sent to\n your email you entered to use the service.\nPlease check in the mailbox.";
            case LANGUAGE_TYPE.JP: return "入力したEメールに送信された\n認証メールを確認していただかなければ\nサービスをご利用いただけません。";
          }
        }
        break;

    //
      case TEXT_TYPE.INPUT_MESSAGE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "스타에게 메시지를 남겨보세요.(최대 68자)";
            case LANGUAGE_TYPE.ENG: return "Leave your message to star.(Up to 124 characters)";
            case LANGUAGE_TYPE.JP: return "スターにメッセージを残してみてください。(最大68字)";
          }
        }
        break;
      case TEXT_TYPE.INPUT_WARNING:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "채팅/댓글을 사용할 때 타인 비방, 욕설 등은 신고 및 제재의 대상이 될 수 있으니 매너를 지켜주세요.";
            case LANGUAGE_TYPE.ENG: return "When using chat/comments, slander and abuse can be subject to reporting and sanctions, so please keep your manners.";
            case LANGUAGE_TYPE.JP: return "チャット/コメントを使用する際は、他人の誹謗、悪口などは通報及び制裁の対象になることがありますのでマナーを守ってください。";
          }
        }
        break;
    //
      case TEXT_TYPE.APPEXIT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "앱을 종료하시겠습니까?";
            case LANGUAGE_TYPE.ENG: return "Do you want to exit the app?";
            case LANGUAGE_TYPE.JP: return "アプリを終了しますか？";
          }
        }
        break;

    //
      case TEXT_TYPE.PLZ_TRY_AGAIN:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "다시 시도해 주세요.";
            case LANGUAGE_TYPE.ENG: return "Please try again.";
            case LANGUAGE_TYPE.JP: return "再び試みてください。";
          }
        }
        break;

    //
      case TEXT_TYPE.ER_ALEADY_SIGNED:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이미 가입된 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account is already subscribed.";
            case LANGUAGE_TYPE.JP: return "すでに会員登録されたアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_SIGN_FAILED:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "가입이 실패되었습니다.";
            case LANGUAGE_TYPE.ENG: return "Failed to join membership.";
            case LANGUAGE_TYPE.JP: return "会員登録に失敗しました。";
          }
        }
        break;
      case TEXT_TYPE.ER_RESIGN_IMPOSSIBLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "가입이 제한되어 있는 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account has restricted subscriptions.";
            case LANGUAGE_TYPE.JP: return "このアカウントは、会員登録ができません。";
          }
        }
        break;
      case TEXT_TYPE.ER_NOT_SIGNED_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "가입이 안되어 있는 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account is not registered.";
            case LANGUAGE_TYPE.JP: return "登録されていないアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_LOGIN_FAILED:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "로그인에 실패했습니다.\n다시 시도해주세요.";
            case LANGUAGE_TYPE.ENG: return "Login failed. Please try again.";
            case LANGUAGE_TYPE.JP: return "ログインに失敗しました。しばらくして、もう一度やり直してください。";
          }
        }
        break;
      case TEXT_TYPE.ER_WITHDRAWN_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이미 탈퇴한 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account has already been withdrawn.";
            case LANGUAGE_TYPE.JP: return "すでに脱退したメンバーです。";
          }
        }
        break;
      case TEXT_TYPE.ER_NOT_ACTIVITY_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "메일인증을 받지 않은 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account has not yet been authenticated by mail.";
            case LANGUAGE_TYPE.JP: return "Eメール認証を受けていないアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_ACCESS_DINYED_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "접속이 제한된 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account has limited access.";
            case LANGUAGE_TYPE.JP: return "接続が制限付きアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_DORMANCY_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "휴면계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This is a dormant account.";
            case LANGUAGE_TYPE.JP: return "休眠アカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_VALIDITY_EXPIRED:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "비밀번호 유효기간이 지난 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account has expired password.";
            case LANGUAGE_TYPE.JP: return "パスワードの有効期限が過ぎたアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_NICKNAME:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "사용할 수 없는 닉네임입니다.";
            case LANGUAGE_TYPE.ENG: return "This nickname cannot be used.";
            case LANGUAGE_TYPE.JP: return "使用できないニックネームです。";
          }
        }
        break;
      case TEXT_TYPE.ER_ACCOUNT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "사용할 수 없는 계정입니다.";
            case LANGUAGE_TYPE.ENG: return "This account is not available.";
            case LANGUAGE_TYPE.JP: return "使用できないアカウントです。";
          }
        }
        break;
      case TEXT_TYPE.ER_EMAIL:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "사용할 수 없는 이메일입니다.";
            case LANGUAGE_TYPE.ENG: return "TThis email is not available.";
            case LANGUAGE_TYPE.JP: return "使用できないEメールです。";
          }
        }
        break;
      case TEXT_TYPE.ER_WRONG_DATA:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "잘못된 데이터입니다.";
            case LANGUAGE_TYPE.ENG: return "Invalid data.";
            case LANGUAGE_TYPE.JP: return "誤ったデータです。";
          }
        }
        break;
      case TEXT_TYPE.ER_EXPIRED_ACCESS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "만료된 접속입니다.";
            case LANGUAGE_TYPE.ENG: return "Expired connection.";
            case LANGUAGE_TYPE.JP: return "満了した接続です。";
          }
        }
        break;
      case TEXT_TYPE.ER_TOO_MANY_REQUEST:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "너무 많은 요청 시도입니다.";
            case LANGUAGE_TYPE.ENG: return "Too many requests attempted";
            case LANGUAGE_TYPE.JP: return "試行された要求が多すぎます。";
          }
        }
        break;

    //store
      case TEXT_TYPE.ER_CANT_BUY_MORE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "이 상품은 더 이상 구매할 수 없습니다.";
            case LANGUAGE_TYPE.ENG: return "This item can not be purchased any more.";
            case LANGUAGE_TYPE.JP: return "この商品は、もはや購入することができません。";
          }
        }
        break;
      case TEXT_TYPE.ER_RECEIPTCHECK_FAILED:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "결제 도중 재고 부족으로 인해 구매하신 카드가 정상적으로 지급되지 않았습니다.결제하신 마켓에 환불 신청을 해주시고, 문제가 해결되지 않을 경우 원핏라이브 고객센터로 연락주세요.(cs@viclip.co.kr)";
            case LANGUAGE_TYPE.ENG: return "The purchase did not proceed normally due to the lack of stock on 1 FT Live card item during your payment process.Please apply for a refund to the market you paid for, and if the problem is not solved, please contact One Fit Live Customer Center.(cs@viclip.co.kr)";
            case LANGUAGE_TYPE.JP: return "決済途中の在庫不足のため、ご購入されたカードが正常に支払われませんでした。決済されたマーケットに払い戻し申請をして、問題が解決されない場合は1FT LIVEカスタマーセンターへメールしてください。（cs@viclip.co.kr）";
          }
        }
        break;
      case TEXT_TYPE.NO_PRODUCTS:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "카드 상품이 없습니다.";
            case LANGUAGE_TYPE.ENG: return "There are no card items for sale.";
            case LANGUAGE_TYPE.JP: return "カード商品がありません。";
          }
        }
        break;

    //
      case TEXT_TYPE.SWEAR:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "부적절한 문구가 포함되어 차단되었습니다.";
            case LANGUAGE_TYPE.ENG: return "The message was blocked with inappropriate phrases or words.";
            case LANGUAGE_TYPE.JP: return "不適切な文言が含まれているため、ブロックされました。";
          }
        }
        break;

    //
      case TEXT_TYPE.NOT_ONAIR:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "방송준비 중입니다.";
            case LANGUAGE_TYPE.ENG: return "Broadcast is in preparation.";
            case LANGUAGE_TYPE.JP: return "放送の準備中です。";
          }
        }
        break;

    //
      case TEXT_TYPE.REPORT_TITLE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "신고하시겠습니까?";
            case LANGUAGE_TYPE.ENG: return "Do you want to report?";
            case LANGUAGE_TYPE.JP: return "申告しますか。";
          }
        }
        break;
      case TEXT_TYPE.REPORT_RESULT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "신고가 완료되었습니다. 다수의 신고를 받은 내용은 자동으로 숨겨지며, 일정 횟수 이상 신고를 받은 경우 채팅이 제한됩니다.";
            case LANGUAGE_TYPE.ENG: return "The chat message has been reported. Chats that receive multiple reports are automatically hidden, and users who receive more than a certain number of reports are restricted from chatting.";
            case LANGUAGE_TYPE.JP: return "申告が完了しました。 多数の届出を受けた内容は自動的に非表示となり、一定回数以上の届出を受けた場合はチャットが制限されます。";
          }
        }
        break;

    //
      case TEXT_TYPE.UNABLE_PLAY:     //문구필요
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "라이브 시청권이 없습니다. 마켓에서 구매하시기 바랍니다.";
            case LANGUAGE_TYPE.ENG: return "You don't have a live ticket. \nPlease buy a tickets at the store to watch.";
            case LANGUAGE_TYPE.JP: return "ライブの視聴権がありません。 ストアで視聴カードを購入してください。";
          }
        }
        break;

    //
      case TEXT_TYPE.MAKEFANFORMYCH:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "좋아하는 채널과 팬을 맺어 마이 채널을 채워보세요.";
            case LANGUAGE_TYPE.ENG: return "Become a fan with your favorite channel and fill up your “My channel”.";
            case LANGUAGE_TYPE.JP: return "お好きなチャンネルとファンを結んで、マイチャンネルを埋めてみてください。";
          }
        }
        break;

    //
      case TEXT_TYPE.MOBILEDATAWARNING:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "모바일 데이터 사용이 제한되어 있어 영상을 재생할 수 없습니다. 모바일 네트워크에서 재생을 허용하시겠습니까?";
            case LANGUAGE_TYPE.ENG: return "You may not watch unless turn celluar data on. Would you like to turn it on?";
            case LANGUAGE_TYPE.JP: return "モバイルデータの使用が制限されており、映像を再生することはできません。 モバイルネットワークで再生を許可しますか。";
          }
        }
        break;

    //
      case TEXT_TYPE.APPUPDATE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "새로운 기능, 더 빠른 속도,\n버그 해결 등이 포함된\n업데이트를 사용할 수 있습니다.";
            case LANGUAGE_TYPE.ENG: return "You must update to the latest version.";
            case LANGUAGE_TYPE.JP: return "アプリを最新バージョンに更新します。";
          }
        }
        break;

    //
      case TEXT_TYPE.NOMYCARD:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "구매한 카드가 없습니다.";
            case LANGUAGE_TYPE.ENG: return "No card purchased.";
            case LANGUAGE_TYPE.JP: return "ご購入いただいたカードがございません。";
          }
        }
        break;

    //
      case TEXT_TYPE.PERMIT:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "혀용";
            case LANGUAGE_TYPE.ENG: return "Permit.";
            case LANGUAGE_TYPE.JP: return "許可。";
          }
        }
        break;

    //
      case TEXT_TYPE.UNLIMITE:
        {
          switch (_country)
          {
            case LANGUAGE_TYPE.KOR: return "무제한";
            case LANGUAGE_TYPE.ENG: return "Unlimited.";
            case LANGUAGE_TYPE.JP: return "無制限。";
          }
        }
        break;
    }
    return "";
  }
}