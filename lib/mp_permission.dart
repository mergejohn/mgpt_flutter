import 'package:flutter/cupertino.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_language.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

/*

2020.09.16

권한 확인용

 */

class mp_permission {
  static void permissionCheck (PermissionGroup _per, BuildContext _c, Function _cancel, Function _agree) async {
    PermissionStatus _permission = await PermissionHandler().checkPermissionStatus(_per);
    print('mp_permission : ' + _permission.toString());
    if (_permission != PermissionStatus.granted){
      if (Platform.isIOS){
        if (_permission == PermissionStatus.unknown){
          permissionRequest(_per, _cancel, _agree);
        }else{
          mp_globalData.showTwoBtPopup(_c, 350, '알림', mp_language.Language(mp_globalData.Language, TEXT_TYPE.IOS_PERMISSION_SETTING_DESC), '취소', '권한설정', iosPermissionSetting);
        }
      }else{
        permissionRequest(_per, _cancel, _agree);
      }
    }else{
      if (_agree != null){_agree();}
    }
  }
  static void permissionRequest (PermissionGroup _per, Function _cancel, Function _agree) async {
    Map<PermissionGroup, PermissionStatus> _permissions = await PermissionHandler().requestPermissions([_per]);
    print('permissionRequest : ' + _permissions[_per].toString());
    if (_permissions[_per] == PermissionStatus.granted){
      if (_agree != null){_agree();}
    }else{
      if (_cancel != null){_cancel();}
    }
  }
  static void iosPermissionSetting (){
    PermissionHandler().openAppSettings();
  }
  static Future<bool> permissionGranted (PermissionGroup _per) async {
    PermissionStatus _permission = await PermissionHandler().checkPermissionStatus(_per);
    if (_permission == PermissionStatus.granted){return true;}
    return false;
  }
}