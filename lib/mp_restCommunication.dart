import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_language.dart';

import 'package:flutter/foundation.dart';

import 'dart:io';
import 'dart:async';

/*

2020.09.16

로그인시 cookie를 받아서 사용한다.

사용중인 통신 함수
RequestPost
RequestLogin
RequestGet

그외는 여러가지 형태의 통신 방식이 있는것을 기록해둔다.

*/


// _M_ 이 붙어있는것이 머지포인트에서 사용될 TYPE 그외는 더미
enum REST_TYPE
{
  NONE,

  //
  GET_M_SPLASH_URL,   //스플래쉬 이미지 URL
  POST_M_APP_VERSION,
  GET_M_HOME,         //메인화면의 정보 (화면안에있는 모든 정보를 한번에 보낸다.
  POST_M_LOG_IN,


  //미구현
  POST_M_VIP_PUSH_AGREE,  //vip push 동의
  POST_M_VIP_PUSH_AGREE_DESC, //vip push 동의 문구
  GET_M_ALRAM,  //알림
  GET_M_MYINFO,     //내정보










  //auth
  POST_SIGN_IN,
  POST_SIGN_OUT,
  POST_LOG_OUT,
  POST_FIND_ID,
  POST_FIND_PASS,
  POST_USERINFO,
  POST_MARKETING_AGREEMENT,
  POST_PROFILE_CHANGE,
  POST_LANGUAGE_CHANGE,

  //media
  POST_VOD_LIST,
  POST_LIVE_LIST,
  POST_NEW_VIDEO_LIST,

  //channel
  POST_MY_CH_LIST,
  POST_NEW_CH_LIST,
  POST_ALL_CH_LIST,
  POST_CH_GUEST,      //채널 방명록
  POST_CH_CLIP,       //채널 영상
  POST_CH_GUEST_COMMENT_REG,      //방명록 등
  POST_CH_MAKE_FAN,
  POST_CH_GUEST_REPORT,

  //chat
  POST_LIVE_CHAT_HISTORY,
  POST_LIVE_CHAT_REG,
  POST_VOD_CHAT_HISTORY,
  POST_VOD_CHAT_REG,
  POST_VOD_CHAT_REPORT,

  //ad
  POST_BANNER_LIST,

  //inapp
  POST_INAPP_LIST,
  POST_INAPP_NEWCARD_LIST,
  POST_INAPP_DETAIL,

  //receipt
  POST_INAPP_RECEPT,
  POST_INAPP_RECEPT_CHECK,
  POST_INAPP_RECEPTED_CHECK,
  POST_INAPP_GETWAITRECEP,

  //card
  POST_MYCARD,
  POST_MYCARD_DETAIL,

  //notice
  POST_NOTICE,

  //상세
  POST_MEDIA_DETAIL,      //vod , newvideo
  POST_LIVE_DETAIL,
  POST_CH_DETAIL,
  POST_NOTICE_DETAIL,

  //mypage
  POST_MYPAGE,
  POST_MARKETING,

  GET_NICK_VALID_CHECK,
  GET_ACCOUNT_VALID_CHECK,
  GET_EMAIL_VALID_CHECK,
  GET_VOD_LIVE_PLAYCOUNT,
  GET_VOD_LIVE_LIKECOUNT,
  GET_NATION_INFO,
  GET_CH_VISIT,
  GET_VERSION,
}

class mp_restCommunication {

  static bool CheckNeedLoadingPopup (REST_TYPE _type){
    if (_type != REST_TYPE.POST_VOD_CHAT_REG && _type != REST_TYPE.POST_LIVE_CHAT_REG &&
        _type != REST_TYPE.POST_LIVE_CHAT_HISTORY && _type != REST_TYPE.POST_VOD_CHAT_HISTORY &&
        _type != REST_TYPE.POST_M_APP_VERSION && _type != REST_TYPE.GET_M_SPLASH_URL){
      return true;
    }
    return false;
  }


  static Map<String, String> headers = {
    'content-type': 'application/x-www-form-urlencoded',
    'user-agent':'marge'
  };
  static void updateCookie(http.Response response) {
    String rawCookie = response.headers['set-cookie'];
    if (rawCookie != null) {
      int index = rawCookie.indexOf(';');
      headers['cookie'] =
      (index == -1) ? rawCookie : rawCookie.substring(0, index);

      print('cookie  !!!!!!!!!!!!!!!!!  ' + headers['cookie']);
      mp_globalData.Cookie = headers['cookie'];
    }
  }

  static void RequestPost (REST_TYPE _type, List<String> _parameter, Function() _error, Function(dynamic) _funcData, BuildContext _c) async {

    if (CheckNeedLoadingPopup(_type)){
      mp_globalData.showLoading(_c);
    }

    try {
      String _url = mp_globalData.Url;
      if (kDebugMode){
        _url = mp_globalData.DebugUrl;
      }

      String _body = ParameterEncode(_type, _parameter);

      final http.Response response = await http.post(
        _url + categoryString(_type),

        headers:<String,String>{
          'User-Agent':'okhttp/3.12.0 marge',
//          'Content-Type': 'application/x-www-form-urlencoded',
//          'Cookie':'JSESSIONID=D2877906EBC27F94199B97C7A6727410',
//          'Accept':'*/*',
//          'Accept-Encoding':'gzip,deflate,br',
//          'Connection':'keep-alive',
//          'Authorization': '$header'
//          'Accept-Type': 'application/json',
//          'Content-Type': 'application/json',
//          'Content-Type': 'application/json; charset=UTF-8',
        },
//        headers: headers,
        body: _body,
      ).timeout(Duration(seconds: 10));

//      updateCookie(response);

      if (response.statusCode == 200) {
        print(response.body);

        if (CheckNeedLoadingPopup(_type)){
          mp_globalData.hideLoading();
        }
        Result_Status_class _value = ParseResultData<Result_Status_class>(response.body, null, Result_Status_class.make).GetDynamic();

        print('status : ' + _value.code + ', rest : ' + _type.toString() + ', data : ' + response.body);

        if (_value.code != 'MRG000'){
          mp_globalData.showErrorPopup(_c, _value.message, _error);
          return;
        }
//        print('result : ' + response.body);

        ParsePostData(_type, response.body, _funcData);
      } else {
        print(response.body);

        print ('fail : ' + _type.toString() + ', ' + response.statusCode.toString());

        if (CheckNeedLoadingPopup(_type)){
          mp_globalData.hideLoading();
        }
//        mp_globalData.showErrorPopup(_c, mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN), _error);
//        if (_error != null){_error();}
        throw Exception('Failed to post');
      }
    }catch (e) {
//      print('---------------');
//      print(_type.toString());
      print(e.toString());
//      mp_globalData.hideLoading();
//      mp_globalData.showErrorPopup(_c, mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN), _error);
    }
  }

  //로그인전용 호출
  //로그인시 statuscode가 302로 호출된다.
  //로그인 쿠키만 받고 넘긴다.
  static void RequestLogin (REST_TYPE _type, List<String> _parameter, Function() _error, Function(dynamic) _funcData, BuildContext _c) async {

    if (CheckNeedLoadingPopup(_type)){
      mp_globalData.showLoading(_c);
    }

    Map<String, String> _map = <String, String> {
      'username':_parameter[0],
      'password':_parameter[1],
    };

    try {
      String _url = mp_globalData.Url;
      if (kDebugMode){
        _url = mp_globalData.DebugUrl;
      }

      final http.Response response = await http.post(
        _url + categoryString(_type),
        headers: headers,
        body: _map,
      ).timeout(Duration(seconds: 10));

      updateCookie(response);     //이곳에서 로그인 쿠키를 받고 넘어간다.

      if (CheckNeedLoadingPopup(_type)){
        mp_globalData.hideLoading();
      }
      if (_funcData != null) {_funcData(null);}

    }catch (e) {
      print(e.toString());
    }
  }

  static void RequestGet (REST_TYPE _type, List<String> _param, Function() _error, Function(REST_TYPE, dynamic) _funcData, BuildContext _c) async {

    if (CheckNeedLoadingPopup(_type)){
      mp_globalData.showLoading(_c);
    }

    String _url = mp_globalData.Url;
    if (kDebugMode){
      _url = mp_globalData.DebugUrl;
    }

    print(_url + categoryString(_type) + makeGetString(_param, _type));
    final response = await http.get(_url + categoryString(_type) + makeGetString(_param, _type), headers: {'cookie':mp_globalData.Cookie});

//    updateCookie(response);

    if (response.statusCode == 200){

      if (CheckNeedLoadingPopup(_type)){
        mp_globalData.hideLoading();
      }

      Result_Status_class _value = ParseResultData<Result_Status_class>(response.body, null, Result_Status_class.make).GetDynamic();
      if (_value.code != 'MRG000'){
        mp_globalData.showErrorPopup(_c, _value.message, _error);
        return;
      }
      ParseGetData(_type, response.body, _funcData);
    }else{

      if (CheckNeedLoadingPopup(_type)){
        mp_globalData.hideLoading();
      }

      if (_error != null){_error();}
      throw Exception('Failed to get : ' + response.statusCode.toString());
    }
  }

  //사용하지않음 기록해두자
  static void RequestHttpClient (REST_TYPE _type, List<String> _parameter, Function() _error, Function(dynamic) _funcData, BuildContext _c) async {

    String _url = mp_globalData.Url;
    if (kDebugMode){
      _url = mp_globalData.DebugUrl;
    }

    HttpClient _client = HttpClient();
    _client.idleTimeout = Duration(seconds: 10);
    String _jsonString = ParameterEncode(_type, _parameter);
//    List<int> bodyBytes = utf8.encode(_jsonString);

    Uri _uri = Uri.parse(_url + categoryString(_type));

    Completer completer = Completer<String>();
    StringBuffer _content = StringBuffer();

    try {
      _client.postUrl(_uri).then((value) => {
//          value.headers.set(HttpHeaders.contentTypeHeader, "application/x-www-form-urlencoded"),
          value.write(_jsonString),
          print('RequestHttpClient !!! : ' + _jsonString),

          value.close().then((_res) => {
            print('Request close !!! : ' + _res.toString()),
//            _res.transform(utf8.decoder).listen((event) { print('Result : ' + event); }),
            _res.transform(utf8.decoder).listen((event) {
              _content.write(event);
              },onDone: () {
                  completer.complete(_content.toString());
            }),
          }),
      });

    }catch (e){
      print('Exception : ' + e.toString());
    }
  }

  //사용하지않음 기록해두자
  static void RequestHttpC () async {
    var client = new http.Client();
    final response =
        await client.post('https://memberdev.mergepoint.co.kr/newapp/signin/authenticate?username=lizzy03@naver.com&password=0000',
//        );
            headers: headers);
//        headers: {'Content-type': 'application/json',
//          'Accept': 'application/json'});

    updateCookie(response);

    if (response.statusCode == 200) {
      print(json.decode(response.body));
    } else {
//        final resGet = await client.get('https://memberdev.mergepoint.co.kr/newapp/signin/authenticate', headers: headers);
//        print(resGet.statusCode.toString());
//      RequestHttpC();
    }
  }

  //사용하지않음 기록해두자
  static void RequestHttpRequest () {
    var request = http.Request('POST', Uri.parse(mp_globalData.DebugUrl + 'newapp/signin/authenticate'));
     Map<String, String> _map = <String,String>{
       'username':'marge',
       'password': 'application/json',
     };
    request.bodyFields = _map;

    request.headers['Cookie'] = 'JSESSIONID=D061703BFE8469AE22D8B4B1B3235531';
    request.headers['Accept'] = '*/*';
    request.headers['Accept-Encoding'] = 'gzip,deflate,br';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Connection'] = 'keep-alive';

    request.send().then((value) => {
      print('RequestHttpRequest'),
      print(value.statusCode.toString())
    });

  }

  //사용하지않음 기록해두자
  static void RequestDioClient (REST_TYPE _type, List<String> _parameter, Function() _error, Function(dynamic) _funcData, BuildContext _c) async {

    try {
      String _url = mp_globalData.Url;
      if (kDebugMode){
        _url = mp_globalData.DebugUrl;
      }

      Map<String, String> _map = <String, String> {
        'username':_parameter[0],
        'password':_parameter[1],
      };

      final Response response = await Dio().post(
        _url + categoryString(_type),

        queryParameters: {
          'username' : _parameter[0],
          'password' : _parameter[1],
        },
//        data: ParameterEncode(_type, _parameter),
        options: Options(
          followRedirects: false,
//          validateStatus: (status){
//            if (status < 500){
//              return true;
//            }
//            return false;
//          },
//          headers:<String,String>{
//            'User-Agent':'marge',
//            'Content-Type': 'application/x-www-form-urlencoded',
//          },
          headers: headers
        ),
      ).timeout(Duration(seconds: 10));

      if (response.statusCode == 200) {
        print(response.data);

        if (CheckNeedLoadingPopup(_type)){
          mp_globalData.hideLoading();
        }
        Result_Status_class _value = ParseResultData<Result_Status_class>(response.data, null, Result_Status_class.make).GetDynamic();

        print('status : ' + _value.code + ', rest : ' + _type.toString() + ', data : ' + response.data);

        if (_value.code != 'MRG000'){
          mp_globalData.showErrorPopup(_c, _value.message, _error);
          return;
        }

        ParsePostData(_type, response.data, _funcData);
      } else {

        print ('fail : ' + _type.toString() + ', ' + response.statusCode.toString());

        if (CheckNeedLoadingPopup(_type)){
          mp_globalData.hideLoading();
        }
        throw Exception('Failed to post');
      }
    }catch (e) {
      print(e.toString());
    }
  }


  static String categoryString(REST_TYPE _type)
  {
    String _result = '';
    switch (_type)
    {
      case REST_TYPE.GET_M_SPLASH_URL:
        _result = 'newapp/getSplash?';
        break;
      case REST_TYPE.POST_M_APP_VERSION:
        _result = 'newapp/version';
        break;
      case REST_TYPE.GET_M_HOME:
        _result = 'newapp/plus/home?';
        break;
      case REST_TYPE.POST_M_LOG_IN:
        _result = 'newapp/signin/authenticate';
//        _result = 'newapp/signin/authenticate?';
        break;
      case REST_TYPE.GET_M_MYINFO:
        _result = 'newapp/plus/account';
        break;

      //2020.09.16 여기까지 현재 사용중 아래는 과거 사용했던 더미











      case REST_TYPE.POST_BANNER_LIST:
        _result = "/ad/banner";
        break;


      case REST_TYPE.POST_SIGN_IN:
        _result = "/user/join/account";
        break;
      case REST_TYPE.POST_SIGN_OUT:
        _result = "/user/leave";
        break;

      case REST_TYPE.POST_FIND_ID:
        _result = "/user/find/account";
        break;
      case REST_TYPE.POST_FIND_PASS:
        _result = "/user/find/password";
        break;
      case REST_TYPE.POST_USERINFO:
        _result = "/user/mypage";
        break;

      case REST_TYPE.POST_LIVE_CHAT_REG:
        _result = "/md/live/chat/send";
        break;
      case REST_TYPE.POST_VOD_CHAT_REG:
        _result = "/md/vod/chat/send";
        break;
      case REST_TYPE.POST_CH_GUEST_REPORT:
        _result = "/ch/chat/report";
        break;
      case REST_TYPE.POST_VOD_CHAT_REPORT:
        _result = "/md/vod/chat/report";
        break;
      case REST_TYPE.POST_LANGUAGE_CHANGE:
        _result = "/user/mypage/change/lang";
        break;
      case REST_TYPE.POST_NEW_VIDEO_LIST:
        _result = "/md/new";
        break;

    //
      case REST_TYPE.POST_MY_CH_LIST:
        _result = "/ch/list/my";
        break;
      case REST_TYPE.POST_NEW_CH_LIST:
        _result = "/ch/list/new";
        break;
      case REST_TYPE.POST_ALL_CH_LIST:
        _result = "/ch/list";
        break;
      case REST_TYPE.POST_CH_CLIP:
        _result = "/ch/media";
        break;
      case REST_TYPE.POST_CH_GUEST:
        _result = "/ch/chat";
        break;
      case REST_TYPE.POST_CH_MAKE_FAN:
        _result = "/ch/count/subscribe";
        break;
      case REST_TYPE.POST_CH_GUEST_COMMENT_REG:
        _result = "/ch/chat/send";
        break;

      case REST_TYPE.POST_PROFILE_CHANGE:
        _result = "/user/mypage/change/profile";
        break;
      case REST_TYPE.POST_VOD_LIST:
        _result = "/md/vod";
        break;
      case REST_TYPE.POST_LIVE_CHAT_HISTORY:
        _result = "/md/live/chat";
        break;
      case REST_TYPE.POST_VOD_CHAT_HISTORY:
        _result = "/md/vod/chat";
        break;
      case REST_TYPE.POST_LIVE_LIST:
        _result = "/md/live";
        break;
      case REST_TYPE.POST_MARKETING_AGREEMENT:
        _result = "/user/mypage/change/marketing";
        break;
      case REST_TYPE.POST_INAPP_LIST:
        _result = "/store/list";
        break;
      case REST_TYPE.POST_INAPP_NEWCARD_LIST:
        _result = "/store/new";
        break;
      case REST_TYPE.POST_INAPP_DETAIL:
        _result = "/store/info";
        break;
      case REST_TYPE.POST_INAPP_RECEPT:
        _result = "/ceritify/rvf";
        break;
      case REST_TYPE.POST_INAPP_RECEPTED_CHECK:
        _result = "/verf/lookup";
        break;
      case REST_TYPE.POST_INAPP_GETWAITRECEP:
        _result = "/verf/user";
        break;
      case REST_TYPE.POST_MYCARD:
        _result = "/card/mycard";
        break;
      case REST_TYPE.POST_MYCARD_DETAIL:
        _result = "/card/mycard";
        break;
      case REST_TYPE.POST_NOTICE:
        _result = "/notice";
        break;

      case REST_TYPE.POST_MEDIA_DETAIL:
        _result = "/md/vod/info";
        break;
      case REST_TYPE.POST_LIVE_DETAIL:
        _result = "/md/live/info";
        break;
      case REST_TYPE.POST_CH_DETAIL:
        _result = "/ch/info";
        break;
      case REST_TYPE.POST_NOTICE_DETAIL:
        _result = "/notice/info";
        break;
      case REST_TYPE.POST_MYPAGE:
        _result = "/user/mypage";
        break;
      case REST_TYPE.POST_MARKETING:
        _result = "/user/mypage/change/marketing";
        break;

      case REST_TYPE.GET_NICK_VALID_CHECK:
        _result = "/user/valid/nick?";
        break;
      case REST_TYPE.GET_ACCOUNT_VALID_CHECK:
        _result = "/user/valid/account?";
        break;
      case REST_TYPE.GET_EMAIL_VALID_CHECK:
        _result = "/user/valid/mail?";
        break;

      case REST_TYPE.GET_VOD_LIVE_LIKECOUNT:
        _result = "/md/count/like?";
        break;
      case REST_TYPE.GET_VOD_LIVE_PLAYCOUNT:
        _result = "/md/count/view?";
        break;
      case REST_TYPE.GET_NATION_INFO:
        _result = "/user/country?";
        break;
      case REST_TYPE.GET_CH_VISIT:
        _result = "/ch/count/view?";
        break;
      case REST_TYPE.GET_VERSION:
        _result = "/check/version?";
        break;
      case REST_TYPE.NONE:
        _result = "/health";
        break;
    }

    return _result;
  }

  static String makeGetString(List<String> _list, REST_TYPE _type)
  {
    if (_list == null) { return ''; }

    String _result = '';
    switch (_type)
    {
      case REST_TYPE.GET_M_SPLASH_URL: _result = "type=" + _list[0]; break;
      case REST_TYPE.GET_M_HOME: _result = 'latitude=' + _list[0] + '&' + 'longitude=' + _list[1]; break;
//      case REST_TYPE.POST_LOG_IN: _result = 'username=' + _list[0] + '&' + 'password=' + _list[1]; break;




      
      case REST_TYPE.GET_NICK_VALID_CHECK:
      {
        _result = "nick=" + _list[0];
      }
      break;
      case REST_TYPE.GET_ACCOUNT_VALID_CHECK:
      {
        _result = "account=" + _list[0];
      }
      break;
      case REST_TYPE.GET_EMAIL_VALID_CHECK:
      {
       _result = "mail=" + _list[0];
      }
      break;

      case REST_TYPE.GET_NATION_INFO:
      {
        _result = "alpha3=" + _list[0] + "&" + "view_chk=" + _list[1];
      }
      break;
      case REST_TYPE.GET_CH_VISIT:
      {
        _result = "user=" + _list[0] + "&" + "uid=" + _list[1];
      }
      break;
      case REST_TYPE.GET_VERSION:
      {
       _result = "device_os=" + _list[0];
      }
      break;
      case REST_TYPE.GET_VOD_LIVE_LIKECOUNT:
      {
        _result = "user=" + _list[0] + "&" + "uid=" + _list[1] + "&" + "type=" + _list[2] + "&" + "stat=" + _list[3];
      }
      break;
      case REST_TYPE.GET_VOD_LIVE_PLAYCOUNT:
      {
        _result = "user=" + _list[0] + "&" + "uid=" + _list[1] + "&" + "type=" + _list[2];
      }
      break;
    }
    return _result;
  }

  static String ParameterEncode (REST_TYPE _type, List<String> _param){
    switch(_type){
      case REST_TYPE.POST_M_APP_VERSION:
        {
          return jsonEncode(<String,String>{
            'os': _param[0],
          });
        }
        break;
      case REST_TYPE.POST_M_LOG_IN:{
        return jsonEncode(<String,String>{
          'username': _param[0],
          'password': _param[1],
        });
      }
      break;








      case REST_TYPE.POST_FIND_ID:{
        return jsonEncode(<String,String>{
          'mail': _param[0],
        });}
        break;
      case REST_TYPE.POST_FIND_PASS:{
        return jsonEncode(<String,String>{
          'account': _param[0],
          'mail': _param[1],
        });
      }
      break;
      case REST_TYPE.POST_SIGN_IN:
        {
          return jsonEncode(<String,String>{
            'account': _param[0],
            'password': _param[1],
            'nick':_param[2],
            'name':_param[3],
            'mail':_param[4],
            'country':_param[5],
            'lang':_param[6],
            'sex':_param[7],
            'birthday':_param[8],
            'device_os':_param[9],
            'device_id':_param[10],
            'terms_mp_service':_param[11],
            'privacy':_param[12],
            'marketing':_param[13]
          });
        }
        break;
      case REST_TYPE.POST_SIGN_OUT:
        {
          return jsonEncode(<String,String>{
            'idx': _param[0],
            'reason': _param[1],
          });
        }
        break;
      case REST_TYPE.POST_NEW_VIDEO_LIST:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
          });
        }
        break;
      case REST_TYPE.POST_LIVE_LIST:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
            'device_os': _param[1],
            'start': _param[2],
            'paging': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_VOD_LIST:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
            'device_os': _param[1],
            'start': _param[2],
            'paging': _param[3],
            'genre': _param[4],
            'order': _param[5],
            'query': _param[6],
            'subscribe': _param[7],
          });
        }
        break;
      case REST_TYPE.POST_NOTICE_DETAIL:
        {
          return jsonEncode(<String,String>{
            'idx': _param[0]
          });
        }
        break;
      case REST_TYPE.POST_CH_DETAIL:
      case REST_TYPE.POST_LIVE_DETAIL:
      case REST_TYPE.POST_MEDIA_DETAIL:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
            'uid': _param[1],
          });
        }
        break;
      case REST_TYPE.POST_LIVE_CHAT_REG:
      case REST_TYPE.POST_VOD_CHAT_REG:
        {
          return jsonEncode(<String,String>{
            'uid': _param[0],
            'user': _param[1],
            'nick': _param[2],
            'chat': _param[3],
            'blind_chk': _param[4],
          });
        }
        break;
      case REST_TYPE.POST_LIVE_CHAT_HISTORY:
      case REST_TYPE.POST_VOD_CHAT_HISTORY:
        {
          return jsonEncode(<String,String>{
            'start': _param[0],
            'user': _param[1],
            'limit': _param[2],
            'uid': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_VOD_CHAT_REPORT:
        {
          return jsonEncode(<String,String>{
            'uid': _param[0],
            'user': _param[1],
            'chat_idx': _param[2],
            'reason': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_MY_CH_LIST:
      case REST_TYPE.POST_NEW_CH_LIST:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
          });
        }
        break;
      case REST_TYPE.POST_ALL_CH_LIST:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
            'start': _param[1],
            'paging': _param[2],
            'order': _param[3],
            'query': _param[4],
          });
        }
        break;
      case REST_TYPE.POST_CH_CLIP:
        {
          return jsonEncode(<String,String>{
            'start': _param[0],
            'user': _param[1],
            'paging': _param[2],
            'uid': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_CH_GUEST:
        {
          return jsonEncode(<String,String>{
            'start': _param[0],
            'user': _param[1],
            'limit': _param[2],
            'uid': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_CH_MAKE_FAN:
        {
          return jsonEncode(<String,String>{
            'user': _param[0],
            'uid': _param[1],
            'subscribe': _param[2],
          });
        }
        break;
      case REST_TYPE.POST_CH_GUEST_REPORT:
        {
          return jsonEncode(<String,String>{
            'uid': _param[0],
            'user': _param[1],
            'chat_idx': _param[2],
            'reason': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_CH_GUEST_COMMENT_REG:
        {
          return jsonEncode(<String,String>{
            'uid': _param[0],
            'user': _param[1],
            'nick': _param[2],
            'chat': _param[3],
          });
        }
        break;
      case REST_TYPE.POST_MYCARD:
        {
          return jsonEncode(<String,String>{
            'idx': _param[0],
            'device_os': _param[1],
          });
        }
        break;

      default:
        break;
    }
    return '';
  }

  static void ParsePostData (REST_TYPE _type, String _data, Function(dynamic) _funcData){
    dynamic _value;

    print(_type);

    switch(_type){
      case REST_TYPE.POST_M_APP_VERSION:
        {
          _value = ParseResultData<Result_AppVersion_class>(_data, 'object', Result_AppVersion_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.POST_M_LOG_IN:{
          _value = ParseResultData<Result_User_class>(_data, 'user', Result_User_class.make).GetDynamic();
        }
        break;





      case REST_TYPE.POST_FIND_ID:{
//          dynamic _d = json.decode(_data);
//          _value = Result_Find_Id_class(_d['user'].toString());
          _value = ParseResultData<Result_Find_Id_class>(_data, 'user', Result_Find_Id_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.POST_CH_GUEST_COMMENT_REG:
      case REST_TYPE.POST_CH_GUEST_REPORT:
      case REST_TYPE.POST_CH_MAKE_FAN:
      case REST_TYPE.POST_FIND_PASS:{
//          dynamic _d = json.decode(_data);
//          _value = Result_Status_class(_d['status'].toString());
        }
        break;
      case REST_TYPE.POST_SIGN_IN:
        {
          _value = json.decode(_data);
        }
        break;
      case REST_TYPE.POST_NOTICE:
        {
          _value = ParseResultData<Result_Notice_class>(_data, 'board', Result_Notice_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_BANNER_LIST :
        {
          _value = ParseResultData<Result_Banner_Detail_class>(_data, 'banner', Result_Banner_Detail_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_NEW_VIDEO_LIST:
      case REST_TYPE.POST_LIVE_LIST:
      {
        _value = ParseResultData<Result_Media_Common_class>(_data, 'media', Result_Media_Common_class.make).GetDynamicList();
      }
      break;
      case REST_TYPE.POST_VOD_LIST:
        {
          _value = ParseResultData<Result_Media_Vod_class>(_data, 'media', Result_Media_Vod_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_NOTICE_DETAIL:
        {
          _value = ParseResultData<Result_Notice_class>(_data, 'board', Result_Notice_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.POST_CH_CLIP:
        {
          _value = ParseResultData<Result_Media_Detail_class>(_data, 'media', Result_Media_Detail_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_MEDIA_DETAIL:
      case REST_TYPE.POST_LIVE_DETAIL:
        {
          _value = ParseResultData<Result_Media_Detail_class>(_data, 'media', Result_Media_Detail_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.POST_CH_DETAIL:
        {
          _value = ParseResultData<Result_Ch_Common_Detail_class>(_data, 'channel', Result_Ch_Common_Detail_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.POST_VOD_CHAT_HISTORY:
      case REST_TYPE.POST_LIVE_CHAT_HISTORY:
        {
          _value = ParseResultData<Result_Chat_Info_class>(_data, 'chat', Result_Chat_Info_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_NEW_CH_LIST:
      case REST_TYPE.POST_MY_CH_LIST:
      case REST_TYPE.POST_ALL_CH_LIST:
        {
          _value = ParseResultData<Result_Ch_Common_Detail_class>(_data, 'channel', Result_Ch_Common_Detail_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_CH_GUEST:
        {
          _value = ParseResultData<Result_Ch_Guest_chat_class>(_data, 'chat', Result_Ch_Guest_chat_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.POST_MYCARD:
        {
//          //debug
//          List<Result_Mycard_group_class> _glist = new List<Result_Mycard_group_class>();
//          for (int i = 0 ; i < 2; i++){
//            Result_Mycard_Detail_class _card = new Result_Mycard_Detail_class(i.toString(), 'identify_code', 'card_name', '0', 'Y', 'start_name', 'start_group', 'start_uid', 1, 1, 'grade', 'price', 'unit', 'present_chk', 'trade_chk', 1, 1, 'front_ld_img', 'front_md_img', 'front_hd_img', 'front_xhd_img', 'front_xxhd_img', 'front_xxxhd_img', 'back_ld_img', 'back_md_img', 'back_hd_img', 'back_xhd_img', 'back_xxhd_img', 'back_xxxhd_img', 'front_detail_img', 'create_dtm', 'transfer_dtm');
//            var cardlist = [_card];
//            Result_Mycard_group_class _g = Result_Mycard_group_class('1', 'group_name', 1, 1, cardlist);
//          }
//          _value = _glist;

          _value = ParseResultData<Result_Mycard_Detail_class>(_data, 'group', Result_Mycard_Detail_class.make).GetDynamic();
        }
        break;
    }
    if (_funcData != null){_funcData(_value);}
  }

  static void ParseGetData (REST_TYPE _type, String _data, Function(REST_TYPE, dynamic) _funcData){

    print('ParseGetData : ' + _data);

    dynamic _value;
    switch(_type){
      case REST_TYPE.GET_M_SPLASH_URL:
        {
          _value = ParseResultData<Result_Splash_class>(_data, null, Result_Splash_class.make).GetDynamic();
        }
        break;
      case REST_TYPE.GET_M_HOME:
        {
          _value = ParseResultData<Result_Home_class>(_data, 'object', Result_Home_class.make).GetDynamic();
        }
        break;


      case REST_TYPE.GET_EMAIL_VALID_CHECK:
      case REST_TYPE.GET_NICK_VALID_CHECK:
      case REST_TYPE.GET_ACCOUNT_VALID_CHECK:
        {
//          dynamic _d = json.decode(_data);
//          _value = Result_Status_class(_d['status'].toString());
        }
        break;
      case REST_TYPE.GET_NATION_INFO:
        {
          _value = ParseResultData<Result_Nation_class>(_data, 'country', Result_Nation_class.make).GetDynamicList();
        }
        break;
      case REST_TYPE.GET_VOD_LIVE_LIKECOUNT:
        {
          _value = ParseResultData<Result_Like_click_class>(_data, 'count', Result_Like_click_class.make).GetDynamic();
        }
        break;
    }
    if (_funcData != null){_funcData(_type, _value);}
  }

  static String IsStatusOkByString(String _status){
    if (_status == "MRG000")
    {
      return 'STATUS_OK';
    }
    if (_status == "SERVER_ERROR")
    {
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "IO_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("파일전송오류");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "PARSE_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("파싱 오류");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "CONTEXT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("컨텍스트 오류");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "JOIN_OVERLAP_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("중복가입자");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_ALEADY_SIGNED);
    }
    if (_status == "JOIN_FAIL_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("가입 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_SIGN_FAILED);
    }
    if (_status == "REJOIN_LIMIT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("재가입 불가상태 에러");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_RESIGN_IMPOSSIBLE);
    }
    if (_status == "NOEXIST_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("미가입자 회원입니다.");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_NOT_SIGNED_ACCOUNT);
    }
    if (_status == "LOGIN_FAIL_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("로그인 실패 오류");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_LOGIN_FAILED);
    }
    if (_status == "LEAVE_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("탈퇴한 회원입니다.");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_WITHDRAWN_ACCOUNT);
    }
    if (_status == "DENIED_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("비활성화 된 회원(메일인증이 되지 않음)");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_NOT_ACTIVITY_ACCOUNT);
    }
    if (_status == "LOCKED_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("잠긴 계정(제재된 계정)");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_ACCESS_DINYED_ACCOUNT);
    }
    if (_status == "ID_EXPIRED_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("휴면처리된 계정");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_DORMANCY_ACCOUNT);
    }
    if (_status == "PW_EXPIRED_USER_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("비밀번호 유효기간 지난 회원");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_VALIDITY_EXPIRED);
    }
    if (_status == "VALID_NAME_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("유효성 검사 오류(별명)");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_NICKNAME);
    }
    if (_status == "VALID_ACCOUNT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("유효성 검사 오류(계정)");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_ACCOUNT);
    }
    if (_status == "VALID_MAIL_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("유효성 검사 오류(메일)");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_EMAIL);
    }
    if (_status == "INVALID_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("유효하지 않은 인자값");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_WRONG_DATA);
    }
    if (_status == "DATA_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("존재하지 않는 데이터");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_WRONG_DATA);
    }
    if (_status == "INIT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("데이터 초기화 오류");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_WRONG_DATA);
    }
    if (_status == "EXPIRE_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("만료되었거나 유효하지 않은 접속");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_EXPIRED_ACCESS);
    }
    if (_status == "REQUEST_TOKEN_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("짧은 시간동안 너무 많은 요청");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_TOO_MANY_REQUEST);
    }
    if (_status == "DB_CONNECT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("DB 접속 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "DB_INSERT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("DB 등록 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "DB_SELECT_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("DB 조회 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "DB_UPDATE_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("DB 수정 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "AWS_FAIL_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("AWS 서버 접속 실패");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
    }
    if (_status == "VALID_DUPLICATE_ERROR")
    {
      //KW_UI_PopUp.INST.ShowPopUp("존재하는 이메일 입니다.");
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_ALREADY_USE);
    }
    if (_status == "JOIN_OVERLAP_ACCOUNT_ERROR"){
      return mp_language.Language(mp_globalData.Language, TEXT_TYPE.ER_ALEADY_SIGNED);
    }

    return mp_language.Language(mp_globalData.Language, TEXT_TYPE.PLZ_TRY_AGAIN);
  }

  static String IsStatusOk(Result_Status_class _data)
  {
    print(_data.code);

    if (_data == null)
    {
      return null;
    }
    return IsStatusOkByString(_data.code);
  }
}








class Result_Status_class {
  String code;
  String message;

  Result_Status_class(this.code, this.message);
  static Result_Status_class make (dynamic json){
    print('Result_Status_class : ' + json['code']);
    return Result_Status_class(json['code'] as String, json['message'] as String);
  }
}

class Result_Splash_class {
  String splashUrl;
  Result_Splash_class (this.splashUrl);
  static Result_Splash_class make (dynamic json){
    return Result_Splash_class(json['object'] as String);
  }
}

class Result_AppVersion_class {
  String version;
  bool require;
  Result_AppVersion_class (this.version, this.require);
  static Result_AppVersion_class make (dynamic json){
    return Result_AppVersion_class(json['version'] as String, json['require'] as bool);
  }
}

class Result_User_class
{
  num id; //Unique value
  String type; //일반회원인지 기업회원인지
  Result_Company_class company; //기업(일반회원일 경우 필드없음)
  String name; //이름
  String birthday; //생년월일
  String gender; //성별
  String phone; //휴대폰번호
  String grade; //회원등급
  String state; //유저상태(탈퇴예정인지)
  Result_Point_class point; //포인트
  String userName; //유저아이디
  String fcmToken; //fcm 토큰
  bool smsAgree;
  bool emailAgree;
  bool marketingAgree; //마케팅동의
  Result_Approval_class approval; //자동승인
  bool subscribe;//구독여부
  bool verification;//본인인증여부(본인인증)
  bool isLocation;
  bool isNewAlarm;         //Y:탈퇴회원

  Result_User_class(this.id, this.type, this.company, this.name, this.birthday, this.gender, this.phone,
      this.grade, this.state, this.point, this.userName, this.fcmToken, this.smsAgree, this.emailAgree, this.marketingAgree, this.approval,
      this.subscribe, this.verification, this.isLocation, this.isNewAlarm);

  static Result_User_class make (dynamic json) {
    if (json == null){return null;}
    return Result_User_class(json['id'] as num, json['type'] as String,
        json['company'], json['name'] as String, json['birthday'] as String,
        json['gender'] as String, json['phone'] as String, json['grade'] as String,
        json['state'] as String, Result_Point_class.make(json['point']), json['userName'] as String,
        json['fcmToken'] as String, json['smsAgree'] as bool, json['emailAgree'] as bool, json['marketingAgree'] as bool, Result_Approval_class.make(json['approval']),
        json['subscribe'] as bool, json['verification'] as bool, json['isLocation'] as bool, json['isNewAlarm'] as bool
    );
  }
}

class Result_Home_class {
  Result_User_class user;
  Result_CastItem_class banner;
  int benefitSale;
  List<Result_FranchiesItem_class> franchiseList;
  List<Result_CastItem_class> castList;
  List<Result_StoreItem_class> myzoneList;
  List<Result_StreetItem_class> streetList;
  List<Result_NoticeItem_class> noticeList;
  List<Result_BrandCategory_class> franchiseCategoryList;
  String bannerUrl;
  Result_UserSaleData_class userSaleData;

  Result_Home_class(this.user, this.banner, this.benefitSale, this.franchiseList, this.castList, this.myzoneList, this.streetList, this.noticeList, this.franchiseCategoryList, this.bannerUrl, this.userSaleData);
  static Result_Home_class make (dynamic json){

    List<Result_FranchiesItem_class> franchiseList = ParseResultData<Result_FranchiesItem_class>(json['franchiseList'], null, Result_FranchiesItem_class.make).GetDynamicList();
    List<Result_CastItem_class> castList = ParseResultData<Result_CastItem_class>(json['castList'], null, Result_CastItem_class.make).GetDynamicList();
    List<Result_StoreItem_class> myzoneList = ParseResultData<Result_StoreItem_class>(json['myzoneList'], null, Result_StoreItem_class.make).GetDynamicList();
    List<Result_StreetItem_class> streetList = ParseResultData<Result_StreetItem_class>(json['streetList'], null, Result_StreetItem_class.make).GetDynamicList();
    List<Result_NoticeItem_class> noticeList = ParseResultData<Result_NoticeItem_class>(json['noticeList'], null, Result_NoticeItem_class.make).GetDynamicList();
    List<Result_BrandCategory_class> franchiseCategoryList = ParseResultData<Result_BrandCategory_class>(json['franchiseCategoryList'], null, Result_BrandCategory_class.make).GetDynamicList();

    return Result_Home_class(Result_User_class.make(json['user']), Result_CastItem_class.make(json['banner']), json['benefitSale'] as int, franchiseList, castList,
        myzoneList, streetList, noticeList, franchiseCategoryList, json['bannerUrl'] as String, Result_UserSaleData_class.make(json['userSaleData']));
  }
}

class Result_FranchiesItem_class {
  int id;
  String name;
  String thumbnailURL;
  Result_SaleItem_class sale;
  bool shopVisible;
  bool newFlag;
  bool plusOnly;
  Result_PaymentMethod_class plusPaymentMethod;
  int plusSale;
  bool favorite;
  int paymentLimitPrice;
  String eventType;
  bool pointMethod;
  bool mobileMethod;

  Result_FranchiesItem_class(this.id, this.name, this.thumbnailURL, this.sale, this.shopVisible, this.newFlag, this.plusOnly, this.plusPaymentMethod, this.plusSale,
      this.favorite, this.paymentLimitPrice, this.eventType, this.pointMethod, this.mobileMethod);
  static Result_FranchiesItem_class make (dynamic json){
    if (json == null){return null;}
    return Result_FranchiesItem_class(json['id'] as int, json['name'] as String, json['thumbnailURL'] as String, Result_SaleItem_class.make(json['sale']), json['shopVisible'] as bool, json['newFlag'] as bool, json['plusOnly'] as bool
        , Result_PaymentMethod_class.make(json['plusPaymentMethod']), json['plusSale'] as int, json['favorite'] as bool, json['paymentLimitPrice'] as int, json['eventType'] as String, json['pointMethod'] as bool, json['mobileMethod'] as bool);
  }
}
class Result_CastItem_class{

  int id;
  String name;
  String thumbnailURL;
  bool detail;

  Result_CastItem_class (this.id, this.name, this.thumbnailURL, this.detail);
  static Result_CastItem_class make (dynamic json){
    if (json == null){return null;}
    return Result_CastItem_class(json['id'] as int, json['name'] as String, json['thumbnailURL'] as String, json['detail'] as bool);
  }
}
class Result_StoreItem_class {
  int id;
  String name;
  String state;
  String businessHour;
  bool isFavorite;
  bool isPartnerShop;
  double distance;
  String address1;
  String address2;
  String thumbnailURL;
  bool isFranchise;
  int franchiseId;
  double latitude;
  double longitude;
  String category;
  String shopCategory;
  Result_FranchiesItem_class franchiseItem;
  bool favorite;
  bool partnerShop;

  Result_StoreItem_class(this.id, this.name, this.state, this.businessHour, this.isFavorite, this.isPartnerShop, this.distance, this.address1, this.address2, this.thumbnailURL, this.isFranchise
      , this.franchiseId, this.latitude, this.longitude, this.category, this.shopCategory, this.franchiseItem, this.favorite, this.partnerShop);
  static Result_StoreItem_class make (dynamic json){
    if (json == null){return null;}
    return Result_StoreItem_class(json['id'] as int, json['name'] as String, json['state'] as String, json['businessHour'] as String, json['isFavorite'] as bool, json['isPartnerShop'] as bool, json['distance'] as double,
      json['address1'] as String, json['address2'] as String, json['thumbnailURL'] as String, json['isFranchise'] as bool, json['franchiseId'] as int, json['latitude'] as double, json['longitude'] as double,
      json['category'] as String, json['shopCategory'] as String, Result_FranchiesItem_class.make(json['franchiseItem']), json['favorite'] as bool, json['partnerShop'] as bool);
  }
}
class Result_StreetItem_class {
  static Result_StreetItem_class make (dynamic json){
    if (json == null){return null;}
    return Result_StreetItem_class();
  }
}
class Result_NoticeItem_class {
  int id;
  String type;
  String title;
  String date;
  Result_NoticeItem_class(this.id, this.type, this.title, this.date);
  static Result_NoticeItem_class make (dynamic json){
    if (json == null){return null;}
    return Result_NoticeItem_class(json['id'] as int, json['type'] as String, json['title'] as String, json['data'] as String);
  }
}
class Result_BrandCategory_class {
  String categoryName;
  int sortPriority;
  String categoryImage;
  String printName;
  int pluseSale;
  String markType;

  Result_BrandCategory_class(this.categoryName, this.sortPriority, this.categoryImage, this.printName, this.pluseSale, this.markType);
  static Result_BrandCategory_class make (dynamic json){
    if (json == null){return null;}
    return Result_BrandCategory_class(json['categoryName'] as String, json['sortPriority'] as int, json['categoryImage'] as String,
      json['printName'] as String, json['pluseSale'] as int, json['markType'] as String, );
  }
}
class Result_UserSaleData_class {
  dynamic mergePlusUserState;
  int saleTotal;
  int returnAmount;
  bool vipServiceOnOff;

  Result_UserSaleData_class (this.mergePlusUserState, this.saleTotal, this.returnAmount, this.vipServiceOnOff);
  static Result_UserSaleData_class make (dynamic json){
    if (json == null){return null;}
    return Result_UserSaleData_class(json['mergePlusUserState'], json['saleTotal'] as int, json['returnAmount'] as int, json['vipServiceOnOff'] as bool);
  }
}
class Result_Company_class {
  static Result_Company_class make (dynamic json){
    if (json == null){return null;}
    return Result_Company_class();
  }
}

class Result_Point_class {

  int total;
  int normal;
  int company;
  Result_Point_class(this.total, this.normal, this.company);
  static Result_Point_class make (dynamic json){
    if (json == null){return null;}
    return Result_Point_class(json['total'] as int, json['normal'] as int, json['company'] as int);
  }
}

class Result_Approval_class {
  static Result_Approval_class make (dynamic json){
    if (json == null){return null;}
    return Result_Approval_class();
  }
}

class Result_PlusUserState_class {

  String membershipState;
  String membershipType;
  String paycoState;

  Result_PlusUserState_class(this.membershipState, this.membershipType, this.paycoState);
  static Result_PlusUserState_class make (dynamic json){
    if (json == null){return null;}
    return Result_PlusUserState_class(json['membershipState'] as String, json['membershipType'] as String, json['paycoState'] as String);
  }
}

class Result_SaleItem_class {
  String type; //할인종류
  String discountType; //할인방식
  String message; //할인 내(ex: 12,000원 할인, 10%할인, 12,000원 페이백, 20% 페이백)
  String discountBanner;
  String saleIconImage;
  int discountPrice;//총 할인 금액

  Result_SaleItem_class(this.type, this.discountType, this.message, this.discountBanner, this.saleIconImage, this.discountPrice);
  static Result_SaleItem_class make(dynamic json){
    if(json == null){return null;}
    return Result_SaleItem_class(json['type'] as String, json['discountType'] as String, json['message'] as String,
      json['discountBanner'] as String, json['saleIconImage'] as String, json['discountPrice'] as int);
  }
}

class Result_PaymentMethod_class {
  String paymentType;
  bool payPriceMobile;
  bool payMenuMobile;
  bool payPricePoint;
  bool payMenuPoint;
  Result_PaymentMethod_class(this.paymentType, this.payPriceMobile, this.payMenuMobile, this.payPricePoint, this.payMenuPoint);
  static Result_PaymentMethod_class make (dynamic json){
    if (json == null){return null;}
    Result_PaymentMethod_class(json['paymentType'] as String, json['payPriceMobile'] as bool, json['payMenuMobile'] as bool, json['payPricePoint'] as bool, json['payMenuPoint'] as bool );
  }
}

















class Result_Find_Id_class {
  String user;

  Result_Find_Id_class(this.user);

  static Result_Find_Id_class make (dynamic json) {
    return Result_Find_Id_class(json['user'].toString());
  }
//  factory Result_Find_Id_class.fromJson(dynamic json) {
//    return Result_Find_Id_class(json['user'].toString());
//  }
}
class Result_Nation_class {
  String iso;
  String country_kor;
  String country_eng;
  String lang;
  String tele_code;
  String alpha3;
  String alpha2;
  String view_chk;

  Result_Nation_class(this.iso, this.country_kor, this.country_eng, this.lang, this.tele_code, this.alpha3, this.alpha2, this.view_chk);

  static Result_Nation_class make (dynamic json) {
    return Result_Nation_class(json['iso'].toString(),json['country_kor'].toString(),json['country_eng'].toString(),
        json['lang'].toString(),json['tele_code'].toString(),json['alpha3'].toString(),json['alpha2'].toString()
        ,json['view_chk'].toString());
  }
//  factory Result_Nation_class.fromJson(dynamic json) {
//    return Result_Nation_class(json['iso'].toString(),json['country_kor'].toString(),json['country_eng'].toString(),
//      json['lang'].toString(),json['tele_code'].toString(),json['alpha3'].toString(),json['alpha2'].toString()
//      ,json['view_chk'].toString());
//  }
}
class Result_Notice_class {
  int idx;
  String title;
  String header;
  String contents;
  String dtm;
  Result_Notice_class(this.idx, this.title, this.header, this.contents, this.dtm);

  static Result_Notice_class make(dynamic json){
    return Result_Notice_class(json['idx'] as int, json['title'].toString(), json['header'].toString(), json['contents'].toString()
        , json['dtm'].toString());
  }
//  factory Result_Notice_class.fromJson (dynamic json){
//    return Result_Notice_class(json['idx'] as int, json['title'].toString(), json['header'].toString(), json['contents'].toString()
//    , json['dtm'].toString());
//  }
}

class Result_Banner_Detail_class
{
  int idx;
  String name;
  String url;
  String link;
  int order_num;
  String link_use;     //Y -> link / N -> uid
  String type;     //vod, channel, notice, store, none
  String uid;
  Result_Banner_Detail_class(this.idx, this.name, this.url, this.link, this.order_num, this.link_use,this.type, this.uid);

  static Result_Banner_Detail_class make (dynamic json){
    return Result_Banner_Detail_class(json['idx'] as int, json['name'].toString(), json['url'].toString(), json['link'].toString(), json['order_num'] as int, json['link_use'].toString()
        , json['type'].toString(), json['uid'].toString());
  }
//  factory Result_Banner_Detail_class.fromJson (dynamic json){
//    return Result_Banner_Detail_class(json['idx'] as int, json['name'].toString(), json['url'].toString(), json['link'].toString(), json['order_num'] as int, json['link_use'].toString()
//        , json['type'].toString(), json['uid'].toString());
//  }
}

class Result_Media_Common_class {
  int idx;
  String name;
  String video;
  String mylike;
  String dtm;
  String type;
  String uid;
  String vertical;
  String thumb;

  int width;
  int height;
  int frame;

  int view_cnt;
  int like_cnt;
  int chat_cnt;

  String ch_uid;
  String ch_name;
  String ch_icon;
  String ch_mysub;
  String watchable;

  int refresh_delay;
  int chat_delay;

  Result_Media_Common_class();
  Result_Media_Common_class.init(this.idx, this.name, this.video, this.mylike, this.dtm, this.type, this.uid, this.vertical, this.thumb, this.width, this.height, this.frame, this.view_cnt, this.like_cnt, this.chat_cnt, this.ch_uid, this.ch_name, this.ch_icon, this.ch_mysub, this.watchable, this.refresh_delay, this.chat_delay);


  static Result_Media_Common_class make (dynamic json){
    return Result_Media_Common_class.init(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['uid'].toString(), json['vertical'].toString(), json['thumb'].toString(),
        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int);
  }
//  factory Result_Media_Common_class.fromJson (dynamic json){
//    return Result_Media_Common_class.init(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['uid'].toString(), json['vertical'].toString(), json['thumb'].toString(),
//        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
//        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int);
//  }
}

class Result_Media_Vod_class extends Result_Media_Common_class {
  int rt;
  Result_Media_Vod_class(int _idx, String _name, String _video, String _mylike, String _dtm, String _type, String _uid, String _vertical, String _thumb, int _width, int _height, int _frame, int _view_cnt, int _like_cnt, int _chat_cnt, String _ch_uid, String _ch_name, String _ch_icon, String _ch_mysub, String _watchable, int _refresh_delay, int _chat_delay, int _rt){
    idx = _idx; name = _name; video = _video; mylike = _mylike; dtm = _dtm; type = _type; uid = _uid; vertical = _vertical; thumb = _thumb; width = _width; height = _height; frame = _frame; view_cnt = _view_cnt;
    like_cnt = _like_cnt; chat_cnt = _chat_cnt; ch_uid = _ch_uid; ch_name = _ch_name; ch_icon = _ch_icon; ch_mysub = _ch_mysub; watchable = _watchable; refresh_delay = _refresh_delay; chat_delay = _chat_delay;
    rt = _rt;
  }

  static Result_Media_Vod_class make (dynamic json){
    return Result_Media_Vod_class(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['uid'].toString(), json['vertical'].toString(), json['thumb'].toString(),
        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int, json['rt'] as int);
  }
//  factory Result_Media_Vod_class.fromJson (dynamic json){
//    return Result_Media_Vod_class(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['uid'].toString(), json['vertical'].toString(), json['thumb'].toString(),
//        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
//        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int, json['rt'] as int);
//  }
}

class Result_Media_Detail_class extends Result_Media_Common_class {
  String artist;
  String genre;
  int rt;
  String access_key;
  String player;
  String official;
  String free_chk;
  String air_on;

  Result_Media_Detail_class(int _idx, String _name, String _video, String _mylike, String _dtm, String _type, String _uid, String _vertical, String _thumb, int _width, int _height, int _frame, int _view_cnt, int _like_cnt, int _chat_cnt, String _ch_uid, String _ch_name, String _ch_icon, String _ch_mysub, String _watchable, int _refresh_delay, int _chat_delay, int _rt
      , String _artist, String _genre, String _access_key, String _player, String _official, String _free_chk, String _air_on){
    idx = _idx; name = _name; video = _video; mylike = _mylike; dtm = _dtm; type = _type; uid = _uid; vertical = _vertical; thumb = _thumb; width = _width; height = _height; frame = _frame; view_cnt = _view_cnt;
    like_cnt = _like_cnt; chat_cnt = _chat_cnt; ch_uid = _ch_uid; ch_name = _ch_name; ch_icon = _ch_icon; ch_mysub = _ch_mysub; watchable = _watchable; refresh_delay = _refresh_delay; chat_delay = _chat_delay;
    rt = _rt; artist = _artist; genre = _genre; access_key = _access_key; player = _player; official = _official; free_chk = _free_chk; air_on = _air_on;
  }

  static Result_Media_Detail_class make(dynamic json){
    return Result_Media_Detail_class(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['uid'].toString(), json['vertical'].toString(), json['thumb'].toString(),
        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int, json['rt'] as int
        , json['artist'].toString(), json['genre'].toString(), json['access_key'].toString(), json['player'].toString(), json['official'].toString(), json['free_chk'].toString(), json['air_on'].toString()
    );
  }

//  factory Result_Media_Detail_class.fromJson (dynamic json){
//    return Result_Media_Detail_class(json['idx'] as int, json['name'].toString(), json['video'].toString(), json['mylike'].toString(), json['dtm'].toString(), json['type'].toString(), json['this'].toString(), json['vertical'].toString(), json['thumb'].toString(),
//        json['width'] as int,json['height'] as int,json['frame'] as int,json['view_cnt'] as int,json['like_cnt'] as int,json['chat_cnt'] as int,
//        json['ch_uid'].toString(),json['ch_name'].toString(),json['ch_icon'].toString(),json['ch_mysub'].toString(),json['watchable'].toString(), json['refresh_delay'] as int,json['chat_delay'] as int, json['rt'] as int
//        , json['artist'].toString(), json['genre'].toString(), json['access_key'].toString(), json['player'].toString(), json['official'].toString(), json['free_chk'].toString(), json['air_on'].toString()
//    );
//  }
}

class Result_Ch_Common_Detail_class
{
  int idx;
  String uid;
  String name;
  String info;
  String type;
  String icon;
  String bg_ld;
  String bg_md;
  String bg_hd;
  String bg_xhd;
  String bg_xxhd;
  String bg_xxxhd;
  int fan_cnt;
  String mysub;
  String dtm;

  int subscribe_cnt;

  Result_Ch_Common_Detail_class(this.idx, this.uid, this.name, this.info, this.type, this.icon, this.bg_ld, this.bg_md, this.bg_hd, this.bg_xhd, this.bg_xxhd, this.bg_xxxhd, this.fan_cnt, this.mysub, this.dtm, this.subscribe_cnt);

  static Result_Ch_Common_Detail_class make(dynamic json){
    return Result_Ch_Common_Detail_class(json['idx'] as int, json['uid'].toString(), json['name'].toString(), json['info'].toString(), json['type'].toString(), json['icon'].toString()
        , json['bg_ld'].toString(), json['bg_md'].toString(), json['bg_hd'].toString(), json['bg_xhd'].toString(), json['bg_xxhd'].toString(), json['bg_xxxhd'].toString()
        , json['fan_cnt'] as int, json['mysub'].toString(), json['dtm'].toString(), json['subscribe_cnt'] as int);
  }
//  factory Result_Ch_Common_Detail_class.fromJson(dynamic json){
//    return Result_Ch_Common_Detail_class(json['idx'] as int, json['uid'].toString(), json['name'].toString(), json['info'].toString(), json['type'].toString(), json['icon'].toString()
//        , json['bg_ld'].toString(), json['bg_md'].toString(), json['bg_hd'].toString(), json['bg_xhd'].toString(), json['bg_xxhd'].toString(), json['bg_xxxhd'].toString()
//        , json['fan_cnt'] as int, json['mysub'].toString(), json['dtm'].toString(), json['subscribe_cnt'] as int);
//  }
}

class Result_Chat_Info_class {
  int idx;
  String uid;
  int user;
  String nick;
  String chat;
  String dtm;
  String blind_chk;

  Result_Chat_Info_class(this.idx, this.uid, this.user, this.nick, this.chat, this.dtm, this.blind_chk);
  static Result_Chat_Info_class make (dynamic json){
    return Result_Chat_Info_class(json['idx'] as int, json['uid'].toString(), json['user'] as int, json['nick'].toString(),
        json['chat'].toString(), json['dtm'].toString(), json['blind_chk'].toString());
  }
}

class Result_Like_click_class {
  String uid;
  String log_day;
  int view_cnt;
  int like_cnt;
  int today_view;
  int today_like;
  int today_cancel;
  Result_Like_click_class(this.uid, this.log_day, this.view_cnt, this.like_cnt, this.today_view, this.today_like, this.today_cancel);
  static Result_Like_click_class make (dynamic json){
    return Result_Like_click_class(json['uid'].toString(), json['log_day'].toString(),
        json['view_cnt'] as int, json['like_cnt'] as int,
        json['today_view'] as int, json['today_like'] as int, json['today_cancel'] as int);
  }
}

class Result_Ch_Guest_chat_class{
  int idx;
  int user;
  int like_cnt;
  int report_cnt;
  String uid;
  String nick;
  String chat;
  String blind_chk;
  String my_like;
  String my_report;
  String dtm;
  Result_Ch_Guest_chat_class(this.idx, this.user, this.like_cnt, this.report_cnt, this.uid, this.nick, this.chat, this.blind_chk, this.my_like, this.my_report, this.dtm);
  static Result_Ch_Guest_chat_class make (dynamic json){
    return Result_Ch_Guest_chat_class(json['idx'] as int, json['user'] as int, json['like_cnt'] as int, json['report_cnt'] as int,
      json['uid'].toString(),json['nick'].toString(),json['chat'].toString(),json['blind_chk'].toString(),json['my_like'].toString(),json['my_report'].toString(),
        json['dtm'].toString()
    );
  }
}

class Result_Mycard_group_class {
  String uid;
  String group_name;
  int group_order;
  int group_card_cnt;
  dynamic mycard;
  Result_Mycard_group_class(this.uid, this.group_name, this.group_order, this.group_card_cnt, this.mycard);
  static Result_Mycard_group_class make (dynamic json){
    return Result_Mycard_group_class(json['uid'].toString(), json['group_name'].toString(), json['group_order'] as int, json['group_card_cnt'] as int, json['mycard']);
  }
}

class Result_Mycard_Detail_class {
  String card_id;
  String identify_code;
  String card_name;
  String card_category;
  String group_chk;
  String start_name;
  String start_group;
  String start_uid;
  int limit_count;
  int card_number;
  String grade;
  String price;
  String unit;
  String present_chk;
  String trade_chk;
  int present_count;
  int trade_count;

  String front_ld_img;
  String front_md_img;
  String front_hd_img;
  String front_xhd_img;
  String front_xxhd_img;
  String front_xxxhd_img;

  String back_ld_img;
  String back_md_img;
  String back_hd_img;
  String back_xhd_img;
  String back_xxhd_img;
  String back_xxxhd_img;

  String front_detail_img;
  String create_dtm;
  String transfer_dtm;

  Result_Mycard_Detail_class(this.card_id, this.identify_code, this.card_name, this.card_category, this.group_chk, this.start_name, this.start_group, this.start_uid, this.limit_count
      , this.card_number, this.grade, this.price, this.unit, this.present_chk, this.trade_chk, this.present_count, this.trade_count, this.front_ld_img, this.front_md_img, this.front_hd_img, this.front_xhd_img,
      this.front_xxhd_img, this.front_xxxhd_img, this.back_ld_img, this.back_md_img, this.back_hd_img, this.back_xhd_img, this.back_xxhd_img, this.back_xxxhd_img, this.front_detail_img, this.create_dtm, this.transfer_dtm);
  static Result_Mycard_Detail_class make (dynamic json){
    return Result_Mycard_Detail_class(json['card_id'].toString(), json['identify_code'].toString(), json['card_name'].toString(), json['card_category'].toString(),
        json['group_chk'].toString(),json['start_name'].toString(),json['start_group'].toString(),json['start_uid'].toString(),json['limit_count'] as int,json['card_number'] as int,json['grade'].toString(),
      json['price'].toString(),json['unit'].toString(),json['present_chk'].toString(),json['trade_chk'].toString(),json['present_count'] as int,json['trade_count'] as int,json['front_ld_img'].toString(),
      json['front_md_img'].toString(),json['front_hd_img'].toString(),json['front_xhd_img'].toString(),json['front_xxhd_img'].toString(),json['front_xxxhd_img'].toString(),json['back_ld_img'].toString(),json['back_md_img'].toString(),
      json['back_hd_img'].toString(),json['back_xhd_img'].toString(),json['back_xxhd_img'].toString(),json['back_xxxhd_img'].toString(),json['front_detail_img'].toString(),json['create_dtm'].toString(),json['transfer_dtm'].toString(),
    );
  }
}

class ParseResultData <T> {
  dynamic dataString;
  String key;
  Function(dynamic) construct;
  ParseResultData(this.dataString, this.key, this.construct);

  dynamic GetDynamic (){
    if (dataString == null){return null;}
    dynamic _value = json.decode(dataString);
    if (key == null) { T _resultnoKey = construct(_value); return _resultnoKey;}
    T _result = construct(_value[key]);
    return _result;
  }
  List<T> GetDynamicList (){
    if (dataString == null){return null;}
//    dynamic _value = json.decode(dataString);
//    List<dynamic> _list = _value[key];

    List<dynamic> _list = dataString;

    List<T> _resultList = new List<T>();
    for (int i = 0 ; i < _list.length ; i++){
      T _result = construct(_list[i]);
      _resultList.add(_result);
    }
    return _resultList;
  }
}

