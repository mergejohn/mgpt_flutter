import 'package:flutter/cupertino.dart';
import 'dart:io';

import 'package:mergepoint/mp_globalData.dart';

/*

2020.09.16

상단 상태바 공간을 비워두기 위함

 */

class mp_safeArea extends StatelessWidget {
  mp_safeArea({this.childWidget});
  Widget childWidget;
  @override
  Widget build(BuildContext context) {
    return SafeArea(left: true, right: true, top: true, bottom: true, child: childWidget,);
  }
}