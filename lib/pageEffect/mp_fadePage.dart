import 'package:flutter/cupertino.dart';
import 'package:mergepoint/pages/mp_mainPage.dart';

/*

2020.9.16

페이지 이동간 연출을 주기위한 로직
테스트중

 */

class mp_fadePage extends CupertinoPageRoute {
  mp_fadePage() : super (builder : (BuildContext context) => new mp_mainPage());

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    // TODO: implement buildPage
    return FadeTransition(opacity: animation, child: mp_mainPage(),);
  }
}