import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mergepoint/pages/mp_loginPage.dart';
import 'package:mergepoint/pages/mp_mainPage.dart';


/*

2020.9.16

현재 위치를 받아오는 페이지를 따로 만들었음
튜토리얼이 끝나고 위치 퍼미션이 granted 되었을때 호출
mp_tutorial_4 에서 호출

 */


enum LOCATION_NEXT_PAGE {
  LOGIN,
  MAIN,
}

class mp_findGpsLocationPage extends StatefulWidget {

  mp_findGpsLocationPage(this.nextPage);
  LOCATION_NEXT_PAGE nextPage;

  @override
  mp_findGpsLocationPageState createState () => mp_findGpsLocationPageState();
}

class mp_findGpsLocationPageState extends State<mp_findGpsLocationPage>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) => {
      Location().getLocation().then((value) => {
        mp_globalData.LATITUDE = value.latitude,
        mp_globalData.LONGITUDE = value.longitude,

        print('founded !!!!!'),

        NextPage(),
    })});
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp (
        debugShowCheckedModeBanner: false,
        home: Scaffold(body: Container(
            color: mainColor,
            child: Stack (
              children: <Widget>[
                Align(alignment: Alignment.bottomCenter, child: Container(padding: EdgeInsets.only(bottom: 60), width: MediaQuery.of(context).size.width, height: 130, child: Stack(children: <Widget>[
                  SpinKitRipple(color: Colors.white,),
                  Align(alignment: Alignment.bottomCenter, child: Text('현재 위치를 가져오는 중...', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),)
                ],),),),
              ],
            )
        ),)
    );
  }

  void NextPage (){
    switch (widget.nextPage){
      case LOCATION_NEXT_PAGE.LOGIN:
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_loginPage()), (route) => false);
        break;
      case LOCATION_NEXT_PAGE.MAIN:
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
        break;
    }
  }
}