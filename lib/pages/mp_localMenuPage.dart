import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_safeArea.dart';

/*

2020.9.16

메뉴가있는 매장 상세페이지
작업중

 */


class mp_localMenuPage extends StatefulWidget{
  @override
  mp_localMenuPageState createState ()=> mp_localMenuPageState();
}
class mp_localMenuPageState extends State<mp_localMenuPage>{

  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == 0){    //스크롤을 맨 및으로 했을 때 상단 연출을 위한 리스너
        print('scroll bottom');
      }else if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent){   //스크롤을 맨 위로 했을 때 상단 연출을 위한 리스너
        print('scroll top');
      }

//      print(_scrollController.position.pixels);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    //상단텝 디버깅
    List<String> _tabs = ["aa", "bb", "cc"];


    // TODO: implement build
    return MaterialApp (
      debugShowCheckedModeBanner: false,
      home: mp_safeArea(childWidget: Scaffold (
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient (
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color.fromARGB(255, 255, 184, 197), Color.fromARGB(255, 251, 137, 175)]
                )
            ),
            child:

                //텝바와 스크롤을 함께 사용할수있게 하는 로직
            DefaultTabController(
              length: _tabs.length, // This is the number of tabs.
              child: NestedScrollView(
                controller: _scrollController,
                physics: const BouncingScrollPhysics(),
                headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverOverlapAbsorber(
                      handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                      sliver: SliverAppBar (
                        pinned: true,
                        stretch: true,                  //상단을 늘려서 확대하는 로직을 테스트 하는중
                        stretchTriggerOffset: 150,      //상단을 늘려서 확대하는 로직을 테스트 하는중
                        onStretchTrigger: (){           //상단을 늘려서 확대하는 로직을 테스트 하는중
                          print('strech!!');
                          return;
                        },
                        expandedHeight: 300.0,        //상단 영역크기
                        forceElevated: innerBoxIsScrolled,
                        flexibleSpace: FlexibleSpaceBar(
                          stretchModes: [
                            StretchMode.zoomBackground,       //상단을 늘릴때 연출 3개중 한개를 사용하면 된다. 테스트
                            StretchMode.blurBackground,
                            StretchMode.fadeTitle,
                          ],
                          background: Container(height: 500, color: Colors.green, child: Center(child: Text('center'),),),
                        ),
                        bottom: TabBar(
                          // These are the widgets to put in each tab in the tab bar.
                          tabs: _tabs.map((String name) => Tab(text: name)).toList(),       //텝바 List<String> _tabs = ["aa", "bb", "cc"];
                        ),
                      ),
                    ),
                  ];
                },

                //텝 전환시 보여지는 하단
                body: TabBarView(
                  children: _tabs.map((String name) {
                    return SafeArea(
                      top: false,
                      bottom: false,
                      child: Builder(
                        builder: (BuildContext context) {
                          return CustomScrollView(
//                        controller: _scrollController,
//                        physics: const BouncingScrollPhysics(),
                            key: PageStorageKey<String>(name),
                            slivers: <Widget>[
                              SliverOverlapInjector(
                                handle:
                                NestedScrollView.sliverOverlapAbsorberHandleFor(
                                    context),
                              ),
                              SliverPadding(
                                padding: const EdgeInsets.all(8.0),
                                sliver: SliverFixedExtentList(
                                  itemExtent: 48.0,
                                  delegate: SliverChildBuilderDelegate(
                                        (BuildContext context, int index) {
                                      return ListTile(
                                        title: Text('Item $index'),         //뷰 인덱스에 따라서 작업할 영역
                                      );
                                    },
                                    childCount: 30,
                                  ),
                                ),
                              ),
                            ],
                          );
                        },
                      ),
                    );
                  }).toList(),
                ),
              ),
            )
        ),),)
    );
  }
}