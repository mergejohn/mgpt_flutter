import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:marquee/marquee.dart';
import 'package:mergepoint/keepAlive/mp_listviewHorizontallAlive.dart';
import 'package:mergepoint/marqueeScroll/mp_marqueeScroll.dart';
import 'package:mergepoint/mp_permission.dart';
import 'package:mergepoint/mp_safeArea.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../mp_restCommunication.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:mergepoint/priceTag/mp_priceTag.dart';
import 'package:flutter/foundation.dart';


/*

2020.09.16
- 배너부분 클릭시 연동 Banner ()
- 베너부분을 스와이프 할때 페이지가 넘어가지 않게 막아야한다.

2020.09.17
- Marquee 사용시 상당히 느려짐 주석처리 -> 고정텍스트를 시용해야겠다.
- 배너부분을 스와이프 할때 listview, carousel_slider 를 사용해봤는데 소용이 없었다.
- 이미지를 캐싱해서 사용한다.  _imageStoreData -> cached_network_image 사용안함

 */


class mp_mainHomePage extends StatefulWidget  {
  @override
  mp_mainHomePageState createState ()=> mp_mainHomePageState();
}
class mp_mainHomePageState extends State<mp_mainHomePage> {

  List<String> _deniedUrl = new List<String>();
  Map<String,Image> _imageStoreData = new Map<String, Image>();

  RefreshController _refreshController = RefreshController();
  Result_Home_class _homeData = null;

  double _headerOffset = 52;      //상단 고정 영역만큼 스크롤길이를 늘려줘야 하단 네이게이션바에 가져지지 않는다.

  int _listCount = 0;
  int _placeHoldCount = 6;

  @override
  void initState() {
    _listCount = _placeHoldCount;

    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      initMainPage();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build (BuildContext context) {
    // TODO: implement build
    return
      SingleChildScrollView (child:

      Column( children: <Widget>[
      Container(width: double.infinity, height: _headerOffset, child: Stack(children: <Widget>[
        Align(alignment: Alignment.centerLeft, child: Padding(padding: EdgeInsets.only(left: 20), child: Image.asset('images/img_merge_logo_typo.png')),),
        Align(alignment: Alignment.centerRight, child: Container(padding: EdgeInsets.only(right: 20), child: GestureDetector(onTap: (){ClickAlram();}, child: Image.asset('images/alram_icon.png'),),),),
        Positioned(top: 14, right: 18, child: Image.asset('images/new_badge.png'),),
      ],),
      ),

        //scroll area (myzone, brand, cast, notice, kakao plus friend)
        Container(height: mp_globalData.MediaQuerySize.height - mp_globalData.TopPadding - _headerOffset -  mp_globalData.BottomNavigationHeight, child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: false,
          controller: _refreshController,
          onRefresh: () async {RequestHomeData(mp_globalData.LATITUDE, mp_globalData.LONGITUDE);},
          child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: _listCount,
              itemBuilder: (_,i) {
                if (_homeData == null) {return SizedBox.shrink();}

                if (i == 0){ return Banner(); }     //고정

                if (i == 1){
                  return _homeData.user == null ? ToLogin() : UserInfo();     //고정
                }
                if (i == 2){ return PayAround(); }      //고정
                if (i == 3){ return Myzone(); }         //고정

                if (i == 4){ return BrandCategoryList();}   //고정

                if (_homeData.castList == null || _homeData.castList.length == 0) {

                  //
                  int _noticeIndex = i - 5;
                  Widget _notice = NoticeDraw(_noticeIndex);
                  if (_notice != null){
                    return _notice;
                  }

                }else{
                  if (i == 5) { return CastTitle(); }
                  int _castIndex = i - 6;
                  if (_castIndex >= _homeData.castList.length){

                    Widget _notice = NoticeDraw(_castIndex);
                    if (_notice != null){
                      return _notice;
                    }

                  }else{
                    print(i.toString() + ' / ' + _homeData.castList.length.toString());
                    return CastList(_castIndex);
                  }
                }

                return PlusFriend();        //고정
              }),
        ),)

      ],),

      );
  }

  void ClickAlram (){
  }

  void initMainPage () async {
    RequestHomeData(mp_globalData.LATITUDE, mp_globalData.LONGITUDE);
  }

  void RequestHomeData (double _lat, double _long){
    mp_restCommunication.RequestGet(REST_TYPE.GET_M_HOME, [_lat.toString(), _long.toString()], () => {mp_globalData.hideLoading()}, HomeDataResult, context);
  }

  void HomeDataResult (REST_TYPE _type, dynamic _data){
    _homeData = _data;
    //전체는 뺀다.
    if (_homeData.franchiseCategoryList != null && _homeData.franchiseCategoryList.length > 0){
      _homeData.franchiseCategoryList.removeAt(0);
    }

    //debug
//    _homeData.castList.clear();

    _listCount = _placeHoldCount;
    if (_homeData.castList != null && _homeData.castList.length > 0){
      _listCount += _homeData.castList.length;
      _listCount++;     //캐스트헤더 하나 추가
    }
    if (_homeData.noticeList != null && _homeData.noticeList.length > 0){
      _listCount += _homeData.noticeList.length;
      _listCount++;     //공지헤더 하나 추가
    }
    setState(() {});
    print('banner : ' + _homeData.bannerUrl);

    _refreshController.refreshCompleted();
  }

  Widget Header () {
    return Container(width: double.infinity, height: 52, child: Stack(children: <Widget>[
        Align(alignment: Alignment.centerLeft, child: Padding(padding: EdgeInsets.only(left: 20), child: Image.asset('images/img_merge_logo_typo.png')),),
        Align(alignment: Alignment.centerRight, child: Container(padding: EdgeInsets.only(right: 20), child: GestureDetector(onTap: (){ClickAlram();}, child: Image.asset('images/alram_icon.png'),),),),
        Positioned(top: 14, right: 18, child: Image.asset('images/new_badge.png'),),
//            Align(alignment: Alignment.topRight, child: Container(padding: EdgeInsets.only(top:10, right: 15), child: Positioned(top: 0, right: 0, child: Image.asset('images/new_badge.png'),)),),
      ],),
      );
  }

  Widget Banner () {

    return _homeData == null ? SizedBox.shrink() : Container(
      width: mp_globalData.MediaQuerySize.width, height: mp_globalData.MediaQuerySize.width / 2.2,
      child:
      InAppWebView(
        initialUrl: _homeData.bannerUrl,
        initialOptions: InAppWebViewGroupOptions(
            crossPlatform: InAppWebViewOptions(
              debuggingEnabled: true,
              useShouldOverrideUrlLoading: true,
//              cacheEnabled: true,
//              javaScriptCanOpenWindowsAutomatically: true,
            )
        ),
        onWebViewCreated: (InAppWebViewController controller) {},
        onLoadStart: (InAppWebViewController controller, String url) {
          print('onLoadStart : ' + url );
        },
        onLoadStop: (InAppWebViewController controller, String url) async {
          print('onLoadStop : ' + url );
        },
        onProgressChanged: (InAppWebViewController controller, int progress) {},
        shouldOverrideUrlLoading: (InAppWebViewController controller, ShouldOverrideUrlLoadingRequest shouldOverrideUrlLoadingRequest) async{

          String paramValue;
          String url = shouldOverrideUrlLoadingRequest.url;
          Uri uri = Uri.parse(url);
          print('shouldOverrideUrlLoading : ' + shouldOverrideUrlLoadingRequest.url);

          //배너 내용에 따라 파싱 필요
          if (url.contains ("http") || url.contains("https")) { //외부 브라우저
//            if (url.contains(mp_globalData.BASE_URL)) {
//              view.loadUrl(url);
//            } else {
//              Intent webBrowserIntent = new Intent(Intent.ACTION_VIEW);
//              webBrowserIntent.setData(uri);
//              activity.startActivity(webBrowserIntent);
//            }
          } else if (url.contains("merge://")) { //화면 넘기기
            if (url.contains('bill')) {
              if (url.contains("Menu")) { //메뉴권
//                paramValue = uri.queryParameters["menuId"];
//                if (isUser()) {
//                  Intent menuIntent = new Intent(activity, MergePlusMenuBarcodeActivity.class);
//                  menuIntent.putExtra(MENU_ID, Long.valueOf(paramValue));
//                  activity.startActivity(menuIntent);
//                } else {
//                  openLoginNeedAlert();   //로그인 창
//                }
              } else if (url.contains("All")) { //메뉴권, 금액권 동시 사용 가능
//                paramValue = uri.getQueryParameter("brandId");
//                MDEBUG.debug("브랜드 아이디 >>> " + paramValue);
//                if (isUser()) {
//                  Intent hybridIntent = new Intent(activity, HybridBrandBarcodeActivity.class);
//                  hybridIntent.putExtra(BRAND_ID, Long.valueOf(paramValue));
//                  activity.startActivity(hybridIntent);
//                } else {
//                  openLoginNeedAlert();
//                }
              } else { //금액권
//                paramValue = uri.getQueryParameter("brandId");
//                MDEBUG.debug("브랜드 아이디 >>> " + paramValue);
//                if (isUser()) {
//                  Intent brandBarcodeIntent = new Intent(activity, BrandBarcodeActivity.class);
//                  brandBarcodeIntent.putExtra(BRAND_ID, Long.valueOf(paramValue));
//                  activity.startActivity(brandBarcodeIntent);
//                } else {
//                  openLoginNeedAlert();
//                }
              }
            } else if (url.contains('plusBenefit')) { //머지플러스 혜택
//              Intent benefitIntent = new Intent(activity, MergePlusBenefitActivity.class);
//              activity.startActivity(benefitIntent);

            } else if (url.contains('castDetail')) { //캐스트 상세
//              paramValue = uri.getQueryParameter("castId");
//              MDEBUG.debug("캐스트 아이디 >>> " + paramValue);
//              Intent intent = new Intent(activity, CastDetailActivity.class);
//              intent.putExtra(CAST_DETAIL, Long.valueOf(paramValue));
//              activity.startActivity(intent);
//              activity.overridePendingTransitionEnter();
            } else if (url.contains('castList')){ //캐스트 더보

            }
          }

          return ShouldOverrideUrlLoadingAction.CANCEL;
        },
      ),



    );
  }

  Widget UserInfo () {

    return Container (
      margin: EdgeInsets.only(left: 24, right: 24, top: 24),
      height: 140,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 1,
            offset: Offset(0, 0), // changes position of shadow
          ),
        ],),
      child: Column (mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[

        Expanded(flex: 6, child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Expanded(flex: 8, child: Container(width: double.infinity, child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Container(padding: EdgeInsets.only(left: 15, top: 0), width: double.infinity, child: Text.rich(
                TextSpan(text:_homeData.user.name, style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, fontSize: 15),
                    children: <TextSpan> [TextSpan(text: '님! 이번에 머지플러스로', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w400, fontSize: 14))])),),
            Container(padding: EdgeInsets.only(left: 15, top: 7), width: double.infinity, child: Text.rich(
                TextSpan(text: NumberFormat("#,###").format(_homeData.userSaleData.saleTotal) + '원', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.blueAccent, fontSize: 18),
                    children: <TextSpan> [TextSpan(text: '이나 할인 받았어요 :)', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w400, fontSize: 15))])),),
          ],),),),
          Expanded(flex: 2, child: Container(padding: EdgeInsets.only(right: 15), width: double.infinity, child: Image.asset('images/arrow_right_big_icon.png', color: Color(0x60000000), scale: 1.3, alignment: Alignment.centerRight,),),),
        ],),),

        Container(padding: EdgeInsets.only(left: 15, right: 15), child: Divider(height:0.3, thickness: 1),),

        Expanded(flex: 3, child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Expanded(flex: 7, child: Container(padding: EdgeInsets.only(left: 24), width: double.infinity, child:Align(alignment: Alignment.centerLeft, child: Text.rich(
              TextSpan(text:'내 머지머니 ', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w400,),
                  children: <TextSpan> [TextSpan(text: NumberFormat("#,###").format(_homeData.user.point.total) + '원', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700))])),),),),
          Expanded(flex: 3, child: Padding(padding: EdgeInsets.only(right: 15), child: GestureDetector(onTap: (){}, child: Align(alignment: Alignment.centerRight,
            child: Container(width: 60, height: 30, decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(0, 0), // changes position of shadow
              ),
            ], borderRadius: BorderRadius.all(Radius.circular(30))), child: Center(child: Text('머니등록', style: TextStyle(fontSize: 10),),),
            ),),),),),
        ],),)
      ],),
    );
  }

  Widget ToLogin () {
    return Container ( padding: EdgeInsets.only(left: 24, top: 24),
      child: Column( children: <Widget>[
        Container(width: mp_globalData.MediaQuerySize.width, child: Text('머지포인트의 다양한 할인혜택을 누려보세요', textAlign: TextAlign.left, style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w300, fontSize: 15, color: Color(0xff3a3a3a)),),) ,
        Row(children: <Widget>[
          Text('회원가입 / 로그인하기', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, fontSize: 30, color: Color(0xff3a3a3a)),),
          Image.asset('images/arrow_right_big_icon.png')
        ],)
      ],),
    );
  }

  Widget PayAround (){
    return Container(padding: EdgeInsets.only(left: 24, right: 24, top: 24), child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
      Text('내 주변 결제하기', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w500, fontSize: 20),),
      GestureDetector(onTap: (){}, child: Image.asset('images/ic_top_refresh.png', color: Color(0x60000000),),)
    ],),);
  }

  Widget Myzone () {
    if (_homeData.myzoneList == null || _homeData.myzoneList.length == 0){
      if (_homeData.user != null){
        String _userName = _homeData.user.name;
        return Padding(padding: EdgeInsets.only(left: 24, top: 15), child:
        Text.rich(
            TextSpan(text:'근처에 매장이 없어요:(\n',
                style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w400, fontSize: 16, color: Color(0xff9f9f9f)),
                children: <TextSpan> [
                  TextSpan(text: _userName, style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, fontSize: 16, color: Colors.black)),
                  TextSpan(text: '님 주변에 더 많은 매장을\n만나보실 수 있도록 저희가 노력할께요!', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w400, fontSize: 16))])));
      }
      return SizedBox.shrink();
    }

    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[

      Padding(padding: EdgeInsets.only(top: 24), child: Container(height: 160, child:

        mp_listviewHorizontallAlive(_homeData.myzoneList.length + 1, MyzoneItemDynamic, 15)

        ,),),

      Container(padding: EdgeInsets.only(left: 24, top: 0), child: GestureDetector(onTap: (){}, child: Row(children: <Widget>[
        Text('더 많은 내 주변 매장 보러가기', style: TextStyle(fontFamily: 'Notosan', color: Colors.grey),),
        SizedBox(width: 7,),
        Image.asset('images/arrow_right_big_icon.png', color: Color(0xffd0d0d0), scale: 1.3,)
      ],),))

    ],);
  }

  Widget MyzoneItemDynamic (int i){
    if (i == 0){
      return Padding(padding: EdgeInsets.only(left: 24,), child: MyzoneItem(i));
    }
    if (i == _homeData.myzoneList.length){
      return SizedBox.shrink();   //맨뒤에 공간 만들어놓자
    }
    return MyzoneItem(i);
  }

  Widget MyzoneItem (int _i){
    if (_homeData.myzoneList[_i].thumbnailURL == null || _deniedUrl.contains(_homeData.myzoneList[_i].thumbnailURL)) return Column (children: <Widget>[
      Container(height: 100, width: 100, decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Color(0xfff2f2f2)),
        child: Center(child: Text('이미지\n준비중', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.grey),),),)
    ],) ;

    print('zoneItem : ' + _homeData.myzoneList[_i].thumbnailURL);

    return Column(children: <Widget>[

      Container (height: 100, width:100, child :
      Center (child: Stack(children: <Widget>[
        ClipRRect(borderRadius: BorderRadius.all(Radius.circular(10)), child:

        ImageLoad(_homeData.myzoneList[_i].thumbnailURL, BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Color(0xfff2f2f2)))

        ),
        Positioned(left: 0, top: 0, child: Padding(padding: EdgeInsets.only(left: 5, top: 5,), child: Column (children: <Widget>[
          _homeData.myzoneList[_i].franchiseItem != null ? PlusDiscountTag(_homeData.myzoneList[_i].franchiseItem.plusSale) : SizedBox.shrink(),
          SizedBox(height: 2,),
          _homeData.myzoneList[_i].franchiseItem != null ? PlusOnlyTag(_homeData.myzoneList[_i].franchiseItem.plusOnly) : SizedBox.shrink(),
        ],),),)
      ],),)
      ),

      SizedBox(height: 5,),

      Container (width:100, height: 20, child: Text (
        _homeData.myzoneList[_i].name,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w500),)),

      Container (width:100, height: 20, child: Text (
        _homeData.myzoneList[_i].address1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontFamily: 'Notosan', color: Color(0xff9f9f9f)),)
      ),
    ],);
  }

  Widget BrandCategoryList () {

    return Column (crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      Container(padding: EdgeInsets.only(left: 24, right: 24, top: 50), child: Text('브랜드 둘러보기', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w500, fontSize: 20),)),
      Padding(padding: EdgeInsets.only(top: 15), child: Container(height: 90, child:

      mp_listviewHorizontallAlive(_homeData.franchiseCategoryList.length + 1, BrandCategoryItemDynamic, 0)

        ,),)
    ],);
  }

  Widget BrandCategoryItemDynamic (int i){
    if (i == 0){
      return Padding(padding: EdgeInsets.only(left: 24,), child: BrandCategoryItem(i));
    }
    if (i == _homeData.franchiseCategoryList.length){
      return SizedBox(width: 24,);   //맨뒤에 공간 만들어놓자
    }
    return BrandCategoryItem(i);
  }

  Widget BrandCategoryItem (int _i){

    print('brand category' + _homeData.franchiseCategoryList[_i].categoryImage);
    if (_homeData.franchiseCategoryList[_i].categoryImage == null) return
      SizedBox(height: 90, width: 90, child: Container(decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xfff2f2f2)),
        child: Center(child: Text('이미지\n준비중', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.grey),),),),);

    return Padding(padding: EdgeInsets.all(5), child: Container (decoration: BoxDecoration(shape: BoxShape.circle,
        boxShadow: [BoxShadow(
          color: Colors.grey,
          spreadRadius: 1,
          blurRadius: 3,
          offset: Offset(0, 0), // changes position of shadow
        ),]), child :
    Padding(padding: EdgeInsets.all(1),child:
    Stack( children: <Widget>[
      SizedBox (width: 90, height: 90, child: ClipOval(child:
      ImageLoad(_homeData.franchiseCategoryList[_i].categoryImage, BoxDecoration(shape: BoxShape.circle, color: Color(0xfff2f2f2))),
      ),),
//        Positioned(left: 0, top: 0, child: PlusDiscountTag(_homeData.franchiseCategoryList[_i].pluseSale),)
      Positioned(left: 0, top: 0, child: PlusDiscountTag(20),)
    ],)

    ),),);
  }

  Widget CastTitle (){
    return Padding(padding: EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 24), child: Column(children: <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        Text('캐스트', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w500, fontSize: 20),),
        Text('전체보기', style: TextStyle(fontFamily: 'Notosan', color: Colors.grey),),
      ],)]));
  }

  Widget CastList (int _index){

    //image size 1081 * 643

    print('cast : ' + _homeData.castList[_index].thumbnailURL);
    return Padding (padding: EdgeInsets.only(left: 24, right: 24, bottom: 15), child: ClipRRect(borderRadius: BorderRadius.all(Radius.circular(10)), child: Container (height: (mp_globalData.MediaQuerySize.width - 48) * 0.6, color: Color(0xfff2f2f2) ,child:
    ImageLoad(_homeData.castList[_index].thumbnailURL, BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10),), color: Color(0xfff2f2f2))),)));
  }

  Widget NoticeDraw (int _index){

    int _noticeIndex = _index - _homeData.castList.length;
    if (_noticeIndex >= 0) {
      if (_homeData.noticeList != null && _homeData.noticeList.length > 0){
        if (_noticeIndex <= _homeData.noticeList.length){
          if (_noticeIndex == 0){
            return NoticeTitle();
          }else{
            return Notice(_noticeIndex-1);
          }
        }
      }
    }

    return null;
  }

  Widget NoticeTitle (){
    return Padding(padding: EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 15), child: Column(children: <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        Text('공지사항', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w500, fontSize: 20),),
        Text('전체보기', style: TextStyle(fontFamily: 'Notosan', color: Colors.grey),),
      ],)]));
  }
  Widget Notice (int index){

    String _title = _homeData.noticeList[index].title;
    String _date = _homeData.noticeList[index].date;
    if (_title != null) {_title = '공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항';}
    if (_date == null) {_date = '111111232145646879765456';}

    return Padding(padding: EdgeInsets.only(left: 24, right: 24, top: 0, bottom: 15), child: Column(children: <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        Expanded(flex: 7, child: Text(_title, overflow: TextOverflow.ellipsis, style: TextStyle(fontFamily: 'Notosan', color:Color(0xff3a3a3a)),),),
        Expanded(flex: 3, child: Text(_date, overflow: TextOverflow.ellipsis, style: TextStyle(fontFamily: 'Notosan', color: Colors.grey),),),
      ],)]));
  }
  Widget PlusFriend (){
    //하단 네이게이션 높이와 헤더 높이도 더해주자
    return Padding(padding: EdgeInsets.only(top: 15), child: Container(padding: EdgeInsets.only(left: 24, right: 24, top: 24, bottom: 24), color: Color(0xfff5f5f5), child: Column (crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: <Widget>[
        Image.asset('images/img_main_kakaoplus.png',),
        Text('자세히 보기', style: TextStyle(fontFamily: 'Notosan', fontSize: 12, color: Colors.grey),),
      ],),
      SizedBox(height: 7,),
      Row(children: <Widget>[
        Text('고객센터 문의하기', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700),),
        SizedBox(width: 8,),
        Image.asset('images/arrow_right_big_icon.png', color: Color(0xffd0d0d0), scale: 1.3,),
      ],),
      SizedBox(height: 15,),
      Text('플러스친구 혹은 메일로 문의주시면\n신속하고 친절한 상담 도와드리겠습니다.', style: TextStyle(fontFamily: 'Notosan', fontSize: 12),),
      SizedBox(height: 18,),
      Text('평일: 오전 10 - 오후 5시 | 주말, 공휴일 휴무', style: TextStyle(fontFamily: 'Notosan', fontSize: 12)),

    ],),),);
  }

  Widget ImageLoad(String _url, Decoration _deco){

    if (_imageStoreData.containsKey(_url)){
      if (_imageStoreData[_url] == null){
        return Container(decoration: _deco, child: Center(child: Text('이미지\n준비중', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.grey),),),);
      }
      return _imageStoreData[_url];
    }

    return new FutureBuilder (
    future: _loadImage(_url),
    builder: (BuildContext context, AsyncSnapshot<Image> image) {
      if (image.hasData) {
        return image.data;  // image is ready
      } else if (image.connectionState == ConnectionState.waiting){
        return Container(decoration: _deco, child: Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFB89AF)),),),);
      } else {
        return Container(decoration: _deco, child: Center(child: Text('이미지\n준비중', style: TextStyle(fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.grey),),),);  // placeholder
      }
    },);
  }

  Future<Image> _loadImage(String _url) async {
    HttpClient httpClient = new HttpClient();
    try {
      var request = await httpClient.getUrl(Uri.parse(_url));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        Image _im = Image.memory(bytes);
        _imageStoreData[_url] = _im;
        return _im;
      }
    }catch (ex){
      _imageStoreData[_url] = null;
      return null;
    }
  }
}