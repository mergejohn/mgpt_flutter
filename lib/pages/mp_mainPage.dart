import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:marquee/marquee.dart';
import 'package:mergepoint/marqueeScroll/mp_marqueeScroll.dart';
import 'package:mergepoint/mp_permission.dart';
import 'package:mergepoint/mp_safeArea.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/pages/mp_mainHomePage.dart';
import 'package:mergepoint/pages/mp_mainLikeShopsPage.dart';
import 'package:mergepoint/pages/mp_mainMyinfoPage.dart';
import 'mp_mainSearchPage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../mp_restCommunication.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:mergepoint/priceTag/mp_priceTag.dart';


/*

2020.09.16
- 배너부분 클릭시 연동 Banner ()

작업중

 */


class mp_mainPage extends StatefulWidget{
  @override
  mp_mainPageState createState ()=> mp_mainPageState();
}
class mp_mainPageState extends State<mp_mainPage>{

  PageController _pageController = PageController();

  pageChangeNotifier _changeNotifier = pageChangeNotifier(0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _changeNotifier.dispose();
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build (BuildContext context) {
    // TODO: implement build
    return MaterialApp (
      debugShowCheckedModeBanner: false,
      home: mp_safeArea(childWidget: Scaffold (
        resizeToAvoidBottomInset: true,
        resizeToAvoidBottomPadding: true,
        body: WillPopScope (
          onWillPop: () {
            return Future.value(false);
          },
          child: PageView (
            controller: _pageController,
//            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              mp_mainHomePage(),
              mp_mainSearchPage(),
              mp_mainLikeShopsPage(),
              mp_mainMyinfoPage(),
            ],


            onPageChanged: (pid) {_changeNotifier.change(pid);},
          ),
        ),

        bottomNavigationBar: SizedBox(height: mp_globalData.BottomNavigationHeight, child: ValueListenableBuilder (
          valueListenable: _changeNotifier,
          builder: (_, _v, __) =>
              BottomNavigationBar(
                items: <BottomNavigationBarItem>[
                  BottomNavigationBarItem (
                      icon: _v == 0 ? Image.asset('images/ic_home_bar_none_active.png', color: Colors.red,) : Image.asset('images/ic_home_bar_none_active.png', color: Colors.grey,),
                      title: Text('홈')
                  ),
                  BottomNavigationBarItem (
                      icon: _v == 1 ? Image.asset('images/ic_search_bar_none_active.png', color: Colors.red,) : Image.asset('images/ic_search_bar_none_active.png', color: Colors.grey,),
                      title: Text('검색')
                  ),
                  BottomNavigationBarItem (
                      icon: _v == 2 ? Image.asset('images/ic_mystore_bar_none_active.png', color: Colors.red,) : Image.asset('images/ic_mystore_bar_none_active.png', color: Colors.grey,),
                      title: Text('찜한매장')
                  ),
                  BottomNavigationBarItem (
                      icon: _v == 3 ? Image.asset('images/ic_myinfo_bar_none_active.png', color: Colors.red,) : Image.asset('images/ic_myinfo_bar_none_active.png', color: Colors.grey,),
                      title: Text('내정보')
                  ),
                ],

                currentIndex: _v,
                selectedItemColor: Color(0xFF656565),
                unselectedItemColor: Color(0xFFFB89AF),
                showUnselectedLabels: true,

                type: BottomNavigationBarType.fixed,
                onTap: (id){ print(id.toString()); _pageController.animateToPage(id, duration: Duration(milliseconds: 150), curve: Curves.linear); },

              ),
        ),) ,
      ),),
    );
  }
}

class pageChangeNotifier extends ValueNotifier<int> {
  int value = 0;
  pageChangeNotifier (int _v) : super(_v);
  void change (int _v){
    value = _v;
    notifyListeners();
  }
}