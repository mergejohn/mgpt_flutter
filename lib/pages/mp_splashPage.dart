import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_safeArea.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/pages/mp_localMenuPage.dart';
import 'package:mergepoint/pages/mp_mainPage.dart';
import 'package:package_info/package_info.dart';
import 'dart:io';
import '../mp_restCommunication.dart';
import 'dart:convert';
import '../mp_language.dart';

/*

2020.09.16

사용하지 않음

 */

class mp_splashPage extends StatefulWidget{
  @override
  mp_splashPageState createState ()=> mp_splashPageState();
}
class mp_splashPageState extends State<mp_splashPage>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (Platform.isIOS){
      mp_globalData.DevicePlatform = 'IOS';
    }

    WidgetsBinding.instance.addPostFrameCallback((timeStamp)
    {
      Future.delayed(Duration(milliseconds: 1000), (){
//        VersionCheck();
      });
    });
  }

  //
  void VersionCheck () {
    GetPackageInfo().then((value) {
      mp_globalData.Version = value.version;
      mp_globalData.readFile('config').then((value)
      {
        if (value != null) {
          mp_globalData.UserData = mp_BasicUserData.fromJson(json.decode(value));
          if (mp_globalData.UserData.id != null && mp_globalData.UserData.pass != null){
            if (mp_globalData.UserData.language == 'ko'){
              mp_globalData.Language = LANGUAGE_TYPE.KOR;
            }else if (mp_globalData.UserData.language == 'jp'){
              mp_globalData.Language = LANGUAGE_TYPE.JP;
            }else{
              mp_globalData.Language = LANGUAGE_TYPE.ENG;
            }
            mp_restCommunication.RequestPost(REST_TYPE.POST_M_LOG_IN, [mp_globalData.UserData.id, mp_globalData.UserData.pass, mp_globalData.DevicePlatform], LoginError, LoginResult, context);
          }else{
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
          }
        }
        else {
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
        }
      });
    });
  }

  Future<PackageInfo> GetPackageInfo () async {
    return await PackageInfo.fromPlatform();
  }

  Future<String> GetDeviceLanguage () async {
    return await Devicelocale.currentLocale;
  }

  Future<void> GetDeviceLanguages () async {
    List lang;
    lang = await Devicelocale.preferredLanguages;
    print('---------> ');
    print(lang);
  }

  void LoginResult (dynamic _data){
    mp_globalData.LoginData = _data;
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
  }

  void LoginError (){
    mp_globalData.deleteFile('config').then((value) => print('delete done'));
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
  }
  //

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp (
      debugShowCheckedModeBanner: false,
      home: Scaffold(body: WillPopScope(
        onWillPop:(){return Future(()=>false);},
        child: Container(width: mp_globalData.MediaQuerySize.width, height: mp_globalData.MediaQuerySize.height, child: Text('splash'),)
        ),
      )
    );
  }
}
