import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:launch_review/launch_review.dart';
import 'package:location/location.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_language.dart';
import 'package:mergepoint/mp_permission.dart';
import 'package:mergepoint/mp_restCommunication.dart';
import 'package:mergepoint/pageEffect/mp_fadePage.dart';
import 'package:mergepoint/pages/mp_mainPage.dart';
import 'package:mergepoint/pages/mp_splashPage.dart';
import 'package:mergepoint/pages/mp_tutorialPage.dart';
import 'dart:convert';

import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:io';


/*

2020.09.16

시작페이지 main 에서 호출

버전확인
스플레시 이미지 다운
튜토리얼
로그인
메인으로

 */

class mp_startPage extends StatefulWidget {
  @override
  mp_startPageState createState ()=> mp_startPageState();
}

class mp_startPageState extends State<mp_startPage> with TickerProviderStateMixin{

  splashShowNotifier _splashshowNotifier = splashShowNotifier(false);
  findLocationNotifier _locationNotifier = findLocationNotifier(false);

  String _splashUrl = '';

  AnimationController animation;
  Animation<double> _fadeInFadeOut;

  SharedPreferences _sharedPreferences;

  String _id = '';
  String _pass = '';
  bool _isFirstAppOpen = true;      //앱을 다운받고 처음 열어보는가? (기본값 설정을 위해 확인)
  bool _isNeedFindLocation = false;
  bool _isSplashAniEnd = false;

  mp_StreamTimeChecker _splashShowTimer = mp_StreamTimeChecker(1500);
  mp_StreamTimeChecker _pageChangeTimer = mp_StreamTimeChecker(1600);

  @override
  void dispose() {
    // TODO: implement dispose
    animation.dispose();
    _splashshowNotifier.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    animation = AnimationController(vsync: this, duration: Duration(milliseconds: 1500),);
    _fadeInFadeOut = Tween<double>(begin: 0.0, end: 1.0).animate(animation);

    //스플레시 이미지 페이드 연출 리스너
    animation.addListener(() {
      if (animation.status == AnimationStatus.completed){
        _isSplashAniEnd = true;
        ShowFindLocationAni();
      }
    });

    WidgetsBinding.instance.addPostFrameCallback((timeStamp)
    {
      SharedPreferences.getInstance().then((value) => {
        mp_globalData.sharedPreferences = _sharedPreferences = value,
        _id = value.getString('id'),
        _pass = value.getString('pass'),

        //debug
        _id = 'lizzy03@naver.com',
        _pass = '0000',
//        if (!kDebugMode){
//          _id = 'mergejohn@mergepoint.co.kr',
//          _pass = 'C!27@ai#uu1',
//        },


        _isFirstAppOpen = value.getBool('isFirstAppOpen') ?? true,
        if (_isFirstAppOpen){
//          value.setBool('isFirstAppOpen', false),   //튜토리얼을 완료했을때 저장하자.
          value.setBool('pushNoti', false),
          value.setBool('smsNoti', false),
          value.setBool('emailNoti', false),
          value.setBool('firebasePushNoti', false),
        },

        print('first open : ' + _isFirstAppOpen.toString()),

        _splashShowTimer.Start(),
        mp_globalData.setScreenSize(MediaQuery.of(context)),
        RequestSplashImage(),
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp (
        debugShowCheckedModeBanner: false,
        home: Scaffold(body: Container(
            color: mainColor,
            child: Stack (
              children: <Widget>[
                Align(alignment: Alignment.center, child: Padding(padding: EdgeInsets.only(bottom: 60), child: Image.asset('images/merge_symbol_logo.png',),),),
                Align(alignment: Alignment.center, child: Padding(padding: EdgeInsets.only(top: 60), child: Image.asset('images/merge_typo_logo.png',),),),
                Align(alignment: Alignment.bottomCenter, child: Padding(padding: EdgeInsets.only(bottom: 38), child: Image.asset('images/merge_com.png',),),),

                ValueListenableBuilder(
                  valueListenable: _splashshowNotifier,
                  builder: (_, bool _v, __) =>
                    Visibility(visible: _v, child: Container(width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height, child:
                        FadeTransition(
                          opacity: _fadeInFadeOut,
                          child: _splashUrl.length > 0 ? Image.network(_splashUrl, fit: BoxFit.fill,) : SizedBox.shrink(),
                        )))
                ),

                ValueListenableBuilder(
                  valueListenable: _locationNotifier,
                  builder: (_, bool _v, __) =>
                    Visibility(visible: _v, child:
                    //spin animation
                    Align(alignment: Alignment.bottomCenter, child: Container(padding: EdgeInsets.only(bottom: 60), width: MediaQuery.of(context).size.width, height: 130, child: Stack(children: <Widget>[
                      SpinKitRipple(color: Colors.white,),
                      Align(alignment: Alignment.bottomCenter, child: Text('현재 위치를 가져오는 중...', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),)
                    ],),),),),
                ),
              ],
            )
        ),)
    );
  }

  void RequestSplashImage () {
    mp_restCommunication.RequestGet(REST_TYPE.GET_M_SPLASH_URL, ['4'], null, SplashResult, context);
  }

  void SplashResult (REST_TYPE _type, dynamic _data){
    Result_Splash_class _splash = _data;
    _splashUrl = _splash.splashUrl;

    Future.delayed(Duration(milliseconds: _splashShowTimer.GetTerm()), (){
      animation.forward();
      _splashshowNotifier.change(true);

      _pageChangeTimer.Start();
      RequestVersion();
    });
  }

  void RequestVersion () {
    mp_restCommunication.RequestPost(REST_TYPE.POST_M_APP_VERSION, [mp_globalData.DevicePlatform], null, VersionResult, context);
  }

  void VersionResult (dynamic _data){
    Result_AppVersion_class _version = _data;
    mp_globalData.Version = _version.version;
    VersionCheck();
  }

  void VersionCheck () {
    GetPackageInfo().then((value) {

      //현재는 버전확인을 하지 말자
//      if (mp_globalData.Version != value.version){
//        mp_globalData.showOneBtAlarmPopupWithFunc (context, 300, mp_language.Language(mp_globalData.Language, TEXT_TYPE.APPUPDATE), ToAppStore);
//        return;
//      }
      //버전통과하면 튜토리얼 또는 권한확인
      if (_isFirstAppOpen){
//        PermissionCheckFirstTime();   //최초 실행시에 튜토리얼로 이동
        ToTutorial();
      }else{
        PermissionCheckForLocationInfo();   //지난 위치 정보 또는 현재위치를 가져오기 위함
      }
    });
  }

  void ToAppStore (){
    LaunchReview.launch(androidAppId:'kr.co.mergepoint.mergeclient', iOSAppId: '1439454507');
  }

  void Login (){
    if (_id == null || _pass == null){ ToMain ();  return;}
    mp_restCommunication.RequestLogin(REST_TYPE.POST_M_LOG_IN, [_id, _pass], null, LoginResult, context);
  }

  void LoginResult (dynamic _data){
    ToMain ();
  }

  Future<PackageInfo> GetPackageInfo () async {
    return await PackageInfo.fromPlatform();
  }

  void ToMain (){
    Future.delayed(Duration(milliseconds: _pageChangeTimer.GetTerm()), (){
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
    });
  }

  void ToTutorial (){
    Future.delayed(Duration(milliseconds: _pageChangeTimer.GetTerm()), (){
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>mp_tutorialPage()), (route) => false);
    });
  }

  //권한을 허용했다면 현재위치를 찾고
  //허용하지 않았다면 지난 내역의 위치를 가져온다.
  void PermissionCheckForLocationInfo (){
    mp_permission.permissionGranted(PermissionGroup.locationWhenInUse).then((value) => {
      if (value == true){

        //현재위치를 찾는다는 애니메이션!!
        _isNeedFindLocation = true,
        ShowFindLocationAni(),

        //권한을 허용했다면 현재위치를 찾고
        Location().getLocation().then((value) => {
          _locationNotifier.change(false),
          mp_globalData.LATITUDE = value.latitude,
          mp_globalData.LONGITUDE = value.longitude,
          Login (),
        }),
      }else{
        //허용하지 않았다면 지난 내역의 위치를 가져온다.
        _sharedPreferences.containsKey('long') ? mp_globalData.LONGITUDE = _sharedPreferences.getDouble('long') : mp_globalData.LONGITUDE = mp_globalData.BASE_LONGITUDE,
        _sharedPreferences.containsKey('lat') ? mp_globalData.LATITUDE = _sharedPreferences.getDouble('lat') : mp_globalData.LATITUDE = mp_globalData.BASE_LATITUDE,
        Login (),
      }
    });
  }

  void ShowFindLocationAni (){
    if (_isNeedFindLocation && _isSplashAniEnd){
      if (!_locationNotifier.value){
        _locationNotifier.change(true);
      }
    }
  }
}

class splashShowNotifier extends ValueNotifier<bool>{
  bool value = false;
  splashShowNotifier(bool _v) : super(_v);
  void change (bool _v){
    value = _v;
    notifyListeners();
  }
}

class findLocationNotifier extends ValueNotifier<bool>{
  bool value = false;
  findLocationNotifier(bool _v) : super(_v);
  void change (bool _v){
    value = _v;
    notifyListeners();
  }
}

