import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_safeArea.dart';

import 'package:mergepoint/tutorials/mp_tutorial_0.dart';
import 'package:mergepoint/tutorials/mp_tutorial_1.dart';
import 'package:mergepoint/tutorials/mp_tutorial_2.dart';
import 'package:mergepoint/tutorials/mp_tutorial_3.dart';
import 'package:mergepoint/tutorials/mp_tutorial_4.dart';
import 'package:preload_page_view/preload_page_view.dart';


/*

2020.09.16

튜토리얼 페이지
현재 엡에서 사용되어지는 이미지와는 다르다
리소스 갱신이 안된듯하다.
추후 확인하자

 */

class mp_tutorialPage extends StatefulWidget{
  @override
  mp_tutorialPageState createState () => mp_tutorialPageState();
}
class mp_tutorialPageState extends State<mp_tutorialPage>{

  PreloadPageController _preloadPageController = PreloadPageController();
  int _pageIndex = 0;

  @override
  void dispose() {
    // TODO: implement dispose
    _preloadPageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //그럴일은 없지만 화면 사이즈 정보가 없을시 여기서 받는다.
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      mp_globalData.setScreenSize(MediaQuery.of(context));
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: mp_safeArea(childWidget:
        Scaffold(
          backgroundColor: Colors.white,
          body: WillPopScope(
            onWillPop: (){      //뒤로가기 금지
              Fluttertoast.showToast(
                msg:'"다음" 버튼을 터치해주세요.',
                toastLength: Toast.LENGTH_SHORT,
              );
              return Future.value(false);
            },
            child:
              PreloadPageView.builder(
                  controller: _preloadPageController,
                  physics: NeverScrollableScrollPhysics(), //스와이프 금지
                  preloadPagesCount: 5,
                  itemBuilder: (_, _i) =>
                    SetPage(_i)
              )
          ),
        ),),
    );
  }

  Widget SetPage (int _i){
    switch(_i){
      case 0 : return mp_tutorial_0(ClickNext);
      case 1 : return mp_tutorial_1(ClickNext);
      case 2 : return mp_tutorial_2(ClickNext);
      case 3 : return mp_tutorial_3(ClickNext);
      case 4 : return mp_tutorial_4();
    }
  }

  void ClickNext (){
    _pageIndex++;
    _preloadPageController.animateToPage(_pageIndex, duration: Duration(milliseconds: 150), curve: Curves.linear);
  }
}

class tutorialChangeNotifier extends ValueNotifier<int> {
  int value = 0;
  tutorialChangeNotifier (int _v) : super(_v);
  void change (int _v){
    value = _v;
    notifyListeners();
  }
}