import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_globalData.dart';

class mp_oneButtonPopup extends StatelessWidget {
  const mp_oneButtonPopup({this.title, this.desc, this.btText, this.popupHeight, this.btClick});

  final String title;
  final String desc;
  final String btText;
  final double popupHeight;

  final oneBtPopUpClickCallBack btClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
          backgroundColor: Colors.transparent,
            body:Center(
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: popupHeight,
                child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Align(alignment: Alignment.topRight, child:
                    Container(width: 50, height: 50, child:
                    IconButton(icon: Icon(Icons.close), onPressed: (){Navigator.pop(context);},)
                    ),
                    ),
                    Text(title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),
                    Padding(padding: EdgeInsets.only(left: 20, right: 20), child: Text(desc, style: TextStyle(fontSize: 15, color: Color(0xff787878)),),),
//                    SizedBox(height: 30,),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFFFB89AF),
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(btText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: btClick,)),
                        ),


                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
  }
}

class mp_oneButtonSingleLinePopup extends StatelessWidget {
  const mp_oneButtonSingleLinePopup({this.title, this.btText, this.popupHeight, this.btClick});

  final String title;
  final String btText;
  final double popupHeight;

  final oneBtPopUpClickCallBack btClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Center(
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: popupHeight,
                child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(width: 50, height: 50, child: Expanded(child: IconButton(icon: Icon(Icons.close), onPressed: (){Navigator.pop(context);},),)),
                    Container(child: Padding(padding: EdgeInsets.only(left: 20, right: 20), child: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),),),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: mainColor,
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(btText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: btClick,)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
  }
}

class mp_oneButtonAlramPopup extends StatelessWidget {
  const mp_oneButtonAlramPopup({this.title, this.btText, this.popupHeight, this.btClick});

  final String title;
  final String btText;
  final double popupHeight;

  final oneBtPopUpClickCallBack btClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Center(
              child: Container(
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: popupHeight + 50,
                child:
                    Stack (children: <Widget>[
                      Positioned(top: 30, child:
                      Container(padding: EdgeInsets.only(top: 30), width: mp_globalData.MediaQuerySize.width * 0.85, height: popupHeight, decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: const Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(child: Container(padding: EdgeInsets.only(left: 20, right: 20), child: Center(child: Text(title, textAlign: TextAlign.center, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),) ,),) ,
                          Row(
                            children: <Widget>[
                              Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      color: mainColor,
                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                                  ),
                                  width: mp_globalData.MediaQuerySize.width * 0.85,
                                  height: 60,
                                  child: FlatButton(child: Text(btText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: btClick,))
                            ],
                          )
                        ],
                      ),
                      ),),
                      Positioned(top: 10, child: Container(width: mp_globalData.MediaQuerySize.width * 0.85, height: 50, decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle), child: Image.asset('images/alram_icon.png', width: 30,),),)
                    ],)
              ),
            )
        )
    );
  }
}

class mp_oneButtonScrollPopup extends StatelessWidget {
  const mp_oneButtonScrollPopup({this.title, this.btText, this.popupHeight, this.btClick});

  final String title;
  final String btText;
  final double popupHeight;

  final oneBtPopUpClickCallBack btClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Center(
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: popupHeight,
                child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Align(alignment: Alignment.topRight, child:
                    Container(width: 50, height: 50, child:
                    IconButton(icon: Icon(Icons.close), onPressed: (){Navigator.pop(context);},)),
                    ),
                    Container(height: popupHeight - 110 - 24, child: Padding(padding: EdgeInsets.only(left: 20, right: 20), child:
                        SingleChildScrollView(child: Text(title.replaceAll(r'\n', '\n'), style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff707070)),),)
                    ,),),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFFFB89AF),
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(btText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: btClick,)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
  }
}

typedef oneBtPopUpClickCallBack = void Function();