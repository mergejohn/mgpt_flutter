import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_language.dart';
import '../mp_globalData.dart';

class mp_signOutPopup extends StatefulWidget{

  const mp_signOutPopup({this.btClick});
  final signOutPopUpClickCallBack btClick;

  @override
  mp_signOutPopupState createState ()=> mp_signOutPopupState();
}
class mp_signOutPopupState extends State<mp_signOutPopup>{
  signOutResonNotifier _reasonSelect = new signOutResonNotifier('0');
  int _selectedReasonId = 0;

  List<DropdownMenuItem<String>> signOutReason (){
    List<DropdownMenuItem<String>> _list = new List<DropdownMenuItem<String>>();
    String _desc = '';
    for (int i = 0 ; i < 5 ; i++){
      switch (i){
        case 0: _desc = mp_language.Language(mp_globalData.Language, TEXT_TYPE.RS_LACKCONTENT); break;
        case 1: _desc = mp_language.Language(mp_globalData.Language, TEXT_TYPE.RS_INCONVENIENT); break;
        case 2: _desc = mp_language.Language(mp_globalData.Language, TEXT_TYPE.RS_NOFAVORITE_STAR); break;
        case 3: _desc = mp_language.Language(mp_globalData.Language, TEXT_TYPE.RS_OUTOFFUN); break;
        case 4: _desc = mp_language.Language(mp_globalData.Language, TEXT_TYPE.RS_EXPENSIVE); break;
      }
      _list.add(new DropdownMenuItem<String>(child: Text(_desc,), value: i.toString(),));
    }
    return _list;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _reasonSelect.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Center(
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: 400,
                child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Align(alignment: Alignment.topRight, child:
                    Container(width: 50, height: 50, child:
                    IconButton(icon: Icon(Icons.close), onPressed: (){Navigator.pop(context);},)
                    ),
                    ),
                    Text(mp_language.Language(mp_globalData.Language, TEXT_TYPE.SIGNOUT_WARNING_ASK), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),
                    Padding(padding: EdgeInsets.only(left: 20, right: 20), child: Text(mp_language.Language(mp_globalData.Language, TEXT_TYPE.SIGNOUT_WARNING), style: TextStyle(fontSize: 15, color: Color(0xff787878)),),),
//                    SizedBox(height: 30,),
                    Padding(padding: EdgeInsets.only(left: 20, right: 20), child: Container(
                        width: double.infinity,
                        child: ValueListenableBuilder(
                            valueListenable: _reasonSelect,
                            builder: (_, _value, __) =>
                                DropdownButton(
                                  isExpanded: true,
                                  value: _value,
                                  items: signOutReason (),
                                  onChanged: (c){
                                    _reasonSelect.select(c);
                                    _selectedReasonId = int.parse(c);
                                  },
                                )
                        )
                    ),),

                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFFFB89AF),
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(mp_language.Language(mp_globalData.Language, TEXT_TYPE.OK), style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed:(){ widget.btClick(_selectedReasonId); Navigator.pop(context, true);},)),
                        ),
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFF999999),
                                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(mp_language.Language(mp_globalData.Language, TEXT_TYPE.CANCLE), style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: (){ Navigator.pop(context, true);},)),
                        )

                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
  }
}

typedef signOutPopUpClickCallBack = void Function(int);
class signOutResonNotifier extends ValueNotifier<String> {
  String value = '0';
  signOutResonNotifier(String _v):super(_v){
    value = _v;
    notifyListeners();
  }
  void select (String _v){
    value = _v;
    notifyListeners();
  }
}