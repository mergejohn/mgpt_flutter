import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_globalData.dart';

//class mp_dialogTest extends StatelessWidget {
//
//  void leftClick (){
//    print('left click');
//  }
//  void rightClick(){
//    print('right click');
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return Scaffold(
//      body: Center (
//        child: FlatButton(child: Text('sadfsf', style: TextStyle(color: Colors.grey),), onPressed: (){
//          showDialog(context: context, builder: (_) =>
////              mp_twoButtonPopup(title: 'title',desc: 'descjkfsdlkjfdgdfkshgdfjghjkdfsghskdfjghksdjfghksdfjghdkfjghskdfjgh',leftBtText: 'left',
////                rightBtText: 'right', popupHeight: 300, leftClick: leftClick, rightClick: rightClick,));
//          mp_oneButtonPopup(title: 'title',desc: 'descjkfsdlkjfdgdfkshgdfjghjkdfsghskdfjghksdjfghksdfjghdkfjghskdfjgh',btText: 'left',
//            popupHeight: 300, btClick: rightClick,));
//        },
//      ),
//    )
//    );
//  }
//}

class mp_twoButtonPopup extends StatelessWidget {
  const mp_twoButtonPopup({this.title, this.desc, this.leftBtText, this.rightBtText, this.popupHeight, this.leftClick, this.rightClick});

  final String title;
  final String desc;
  final String leftBtText;
  final String rightBtText;
  final double popupHeight;

  final twoBtPopUpClickCallBack leftClick;
  final twoBtPopUpClickCallBack rightClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body:Center(
          child: Container(

            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: const Color(0xFFFFFFFF),
              borderRadius: BorderRadius.circular(10),
            ),
            width: mp_globalData.MediaQuerySize.width * 0.85,
            height: 300,
            child:
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(width: mp_globalData.MediaQuerySize.width * 0.85, child:
                  Align(alignment: Alignment.centerLeft, child: Container(padding: EdgeInsets.only(left: 20, top: 20), child: Text(title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),),),
                ),
                Expanded(child: Container(child: Center(child: Padding(padding: EdgeInsets.only(left: 20, right: 20), child: Text(desc, style: TextStyle(fontSize: 15, color: Color(0xff787878)),),),),) ,),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: const Color(0xFFFB89AF),
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10))
                          ),
                          height: 60,
                          child: FlatButton(child: Text(leftBtText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: leftClick,)),
                    ),
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: const Color(0xFF999999),
                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(10))
                          ),
                          height: 60,
                          child: FlatButton(child: Text(rightBtText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: (){if (rightClick != null){rightClick();}},)),
                    )

                  ],
                )
              ],
            ),
          ),
        )
      )
    );
  }
}

class mp_twoButtonSingleLinePopup extends StatelessWidget {
  const mp_twoButtonSingleLinePopup({this.title, this.leftBtText, this.rightBtText, this.popupHeight, this.leftClick, this.rightClick});

  final String title;
  final String leftBtText;
  final String rightBtText;
  final double popupHeight;

  final twoBtPopUpClickCallBack leftClick;
  final twoBtPopUpClickCallBack rightClick;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Align(alignment: Alignment.center,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body:Center(
              child: Container(

                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: const Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.circular(10),
                ),
                width: mp_globalData.MediaQuerySize.width * 0.85,
                height: popupHeight,
                child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Align(alignment: Alignment.topRight, child:
                    Container(width: 50, height: 50, child:
//              FlatButton(child: Text('X', style: TextStyle(fontSize: 30),), onPressed: (){Navigator.pop(context);},),
                    IconButton(icon: Icon(Icons.close), onPressed: (){Navigator.pop(context);},)
                    ),
                    ),
                    Container(height: 60, child: Text(title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff707070)),),),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFFFB89AF),
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(leftBtText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: leftClick,)),
                        ),
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: const Color(0xFF999999),
                                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(10))
                              ),
                              height: 60,
                              child: FlatButton(child: Text(rightBtText, style: TextStyle(color: Colors.white, fontSize: 17), ), onPressed: (){if (rightClick != null){rightClick();}},)),
                        )

                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
  }
}

typedef twoBtPopUpClickCallBack = void Function();