import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget PlusDiscountTag (int _price) {
  if (_price == 0 || _price == null) {return SizedBox.shrink();}

  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(30)),
      color: Colors.white,
      border: Border.all(color: Colors.blue, width: 1)
    ),
    width: 40, height: 15, child: Row (crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Image.asset('images/ic_patch_plus.png', scale: 1.3,),
      SizedBox(width: 3,),
      Padding(padding: EdgeInsets.only(top: 1), child: Text(_price.toString() + '%', textAlign: TextAlign.center, style: TextStyle(fontSize: 8, fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.blue),),)
  ],),);
}

Widget PlusOnlyTag (bool _isPlus) {
  if (!_isPlus) {return SizedBox.shrink();}

  return Container(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(30)),
        color: Colors.blue,
    ),
    width: 40, height: 15, child: Row (crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
    Image.asset('images/ic_patch_plus.png', scale: 1.3, color: Colors.white,),
    SizedBox(width: 3,),
    Padding(padding: EdgeInsets.only(top: 1), child: Text('ONLY', style: TextStyle(fontSize: 7, fontFamily: 'Notosan', fontWeight: FontWeight.w700, color: Colors.white),),)

  ],),);
}