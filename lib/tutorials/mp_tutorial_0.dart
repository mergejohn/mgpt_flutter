import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class mp_tutorial_0 extends StatelessWidget {

  mp_tutorial_0(this.nextClick);
  final Function nextClick;
  bool _isClicked = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column (
      children: <Widget>[

        Padding (padding: EdgeInsets.only(top: 59, left: 23), child: Container(width: MediaQuery.of(context).size.width, child: Text('앱 접근권한 안내', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff3a3a3a), fontSize: 30, fontFamily: 'Notosan', fontWeight: FontWeight.w700),),),),
        Padding (padding: EdgeInsets.only(top: 6, left: 23), child: Container(width: MediaQuery.of(context).size.width, child: Text('머지포인트 앱을 이용하기 위해서는\n다음의 앱 권한을 허용해주세요.', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w300),),),),
        Container(margin: EdgeInsets.only(left: 23, top: 15, right: 23), child: Divider(),),
        Padding (padding: EdgeInsets.only(top: 26, left: 23), child: Container(width: MediaQuery.of(context).size.width, child: Text('필수적 접근권한', textAlign: TextAlign.start, style: TextStyle(color: Color(0xfff92d49), fontSize: 17.0, fontFamily: 'Notosan', fontWeight: FontWeight.w500),),),),
        Padding (padding: EdgeInsets.only(top: 17, left: 23), child:
          Row(children: <Widget>[
            Container(height:40, width:40, decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xfff92d49),), child: Image.asset('images/phone_r_icon.png', scale: 0.9,)),
            SizedBox(width: 15,),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Text('기기 및 앱 기록', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w500),),
              Text('앱 서비스 최적화 및 기기오류', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w300),)
            ],)
          ])),

        Padding (padding: EdgeInsets.only(top: 26, left: 23), child: Container(width: MediaQuery.of(context).size.width, child: Text('선택적 접근권한', textAlign: TextAlign.start, style: TextStyle(color: Color(0xfff92d49), fontSize: 17.0, fontFamily: 'Notosan', fontWeight: FontWeight.w500),),),),
        Padding (padding: EdgeInsets.only(top: 17, left: 23), child:
        Row(children: <Widget>[
          Container(height:40, width:40, decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xfff92d49),), child: Image.asset('images/bell_r_icon.png', scale: 0.9,)),
          SizedBox(width: 15,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('알림', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w500),),
            Text('푸시알림 등록 및 수신', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w300),)
          ],)
        ])),
        Padding (padding: EdgeInsets.only(top: 17, left: 23), child:
        Row(children: <Widget>[
          Container(height:40, width:40, decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0xfff92d49),), child: Center(child: Image.asset('images/gps_r_icon.png', scale: 0.9,),) ),
          SizedBox(width: 15,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('GPS', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w500),),
            Text('내 위치 및 점포까지 거리 표시', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w300),)
          ],)
        ])),


        Expanded(child: Container( padding: EdgeInsets.only(top: 26, left: 23, right: 23), child: Stack(children: <Widget>[
          Positioned(left: 0, top: 0, child: Container(height:20, width:20, child: Image.asset('images/info_gray_icon.png', scale: 0.9,)),),
          Positioned(left: 30, top: 0, child: Container(width: MediaQuery.of(context).size.width - 76, child:
          Text('선택적 접근권한에 동의하지 않으셔도 앱을 이용하실 수 있으나, 일부 서비스의 이용이 제한될 수 있습니다.', textAlign: TextAlign.start, style: TextStyle(color: Color(0xff727272), fontSize: 15.0, fontFamily: 'Notosan', fontWeight: FontWeight.w300),),) ,),
          Align(alignment: Alignment.bottomCenter, child:
              Column (mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                GestureDetector(onTap: (){ClickOk(); }, behavior: HitTestBehavior.opaque, child: Container(width: MediaQuery.of(context).size.width, height: 80, decoration: BoxDecoration(color: Colors.red, borderRadius:  BorderRadius.all(Radius.circular(15))), child: Center(child: Text('확인', style: TextStyle(color: Colors.white, fontSize: 20, fontFamily: 'Notosan', fontWeight: FontWeight.w700),),),),),
                SizedBox(height: 24,)
              ],)
          )
        ],),)),
    ],);
  }

  void ClickOk (){
    if (_isClicked){return;}
    nextClick();
    _isClicked = true;
  }
}