
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_permission.dart';
import 'package:permission_handler/permission_handler.dart';

class mp_tutorial_1 extends StatelessWidget {

  mp_tutorial_1(this.nextClick);
  final Function nextClick;
  bool _isClicked = false;

  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    // TODO: implement build
    return Column (
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Image.asset('images/img_tutorial_1.png'),

        Expanded(child: Container(padding: EdgeInsets.only(left: 24, right: 24), child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(onTap: (){ClickOk(); }, behavior: HitTestBehavior.opaque, child: Container( width: MediaQuery.of(context).size.width * 0.5 - 48, child: Center(child: Text('나중에 설정하기', style: TextStyle(color: Color(0xff727272), fontFamily: 'Notosan', fontWeight: FontWeight.w300),),),),),
            GestureDetector(onTap: (){RequestPermission(); }, behavior: HitTestBehavior.opaque, child: Container(width: MediaQuery.of(context).size.width * 0.5 - 48, height: 60, decoration: BoxDecoration(color: Colors.red, borderRadius:  BorderRadius.all(Radius.circular(15))), child: Center(child: Text('다음', style: TextStyle(color: Colors.white, fontSize: 20, fontFamily: 'Notosan', fontWeight: FontWeight.w400),),),),),
          ],
        ),),)
      ],);
  }

  void ClickOk (){
    if (_isClicked){return;}
    nextClick();
    _isClicked = true;
  }

  void RequestPermission (){
    mp_permission.permissionCheck(PermissionGroup.locationWhenInUse, _context, ()=> ClickOk(), ()=> ClickOk());
  }
}