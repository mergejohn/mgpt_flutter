import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class mp_tutorial_3 extends StatelessWidget {

  mp_tutorial_3(this.nextClick);
  final Function nextClick;
  bool _isClicked = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column (
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Image.asset('images/img_tutorial_3.png'),

        Expanded(child: Container(padding: EdgeInsets.only(left: 24, right: 24), child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(onTap: (){ }, behavior: HitTestBehavior.opaque, child: Container( width: MediaQuery.of(context).size.width * 0.5 - 48, child: Center(child: Text(' ', style: TextStyle(color: Color(0xff727272), fontFamily: 'Notosan', fontWeight: FontWeight.w300),),),),),
            GestureDetector(onTap: (){ClickOk(); }, behavior: HitTestBehavior.opaque, child: Container(width: MediaQuery.of(context).size.width * 0.5 - 48, height: 60, decoration: BoxDecoration(color: Colors.red, borderRadius:  BorderRadius.all(Radius.circular(15))), child: Center(child: Text('다음', style: TextStyle(color: Colors.white, fontSize: 20, fontFamily: 'Notosan', fontWeight: FontWeight.w400),),),),),
          ],
        ),),)
      ],);
  }

  void ClickOk (){
    if (_isClicked){return;}
    nextClick();
    _isClicked = true;
  }
}