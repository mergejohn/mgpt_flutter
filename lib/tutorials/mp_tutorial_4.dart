import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mergepoint/mp_globalData.dart';
import 'package:mergepoint/mp_permission.dart';
import 'package:mergepoint/pages/mp_findGpsLocationPage.dart';
import 'package:mergepoint/pages/mp_loginPage.dart';
import 'package:mergepoint/pages/mp_mainPage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class mp_tutorial_4 extends StatelessWidget {

  bool _isClicked = false;
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    // TODO: implement build
    return Column (
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Image.asset('images/img_tutorial_4.png'),

        Expanded(child: Container(padding: EdgeInsets.only(left: 24, right: 24), child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(onTap: (){ToMainClick(); }, behavior: HitTestBehavior.opaque, child: Container( width: MediaQuery.of(context).size.width * 0.5 - 48, child: Center(child: Text('둘러보기', style: TextStyle(color: Color(0xff727272), fontFamily: 'Notosan', fontWeight: FontWeight.w300),),),),),
            GestureDetector(onTap: (){LoginClick(); }, behavior: HitTestBehavior.opaque, child: Container(width: MediaQuery.of(context).size.width * 0.5 - 48, height: 60, decoration: BoxDecoration(color: Colors.red, borderRadius:  BorderRadius.all(Radius.circular(15))), child: Center(child: Text('로그인˙회원가입', style: TextStyle(color: Colors.white, fontSize: 17, fontFamily: 'Notosan', fontWeight: FontWeight.w400),),),),),
          ],
        ),),)
      ],);
  }

  //둘러보기
  void ToMainClick (){
    if (_isClicked){return;}
    CheckLocation(false);
    _isClicked = true;
  }

  //로그인
  void LoginClick (){
    if (_isClicked){return;}
    CheckLocation(true);
    _isClicked = true;
  }

  void CheckLocation (bool _isNextLogin) {

    //권한 확인해서 이동하자
    mp_permission.permissionGranted(PermissionGroup.locationWhenInUse).then((value) => {
        if (value == true){
          if (_isNextLogin){
            Navigator.pushAndRemoveUntil(_context, MaterialPageRoute(builder: (context)=>mp_findGpsLocationPage(LOCATION_NEXT_PAGE.LOGIN)), (route) => false),
          }else{
            Navigator.pushAndRemoveUntil(_context, MaterialPageRoute(builder: (context)=>mp_findGpsLocationPage(LOCATION_NEXT_PAGE.MAIN)), (route) => false),
          }
        }else{
          if (_isNextLogin){
            ToLogin(),
          }else{
            ToMain(),
          }
        },

        //다음부터 튜토리얼보는것을 막자!!
        if (mp_globalData.sharedPreferences == null){
          SharedPreferences.getInstance().then((value) => {
            mp_globalData.sharedPreferences = value,
            mp_globalData.sharedPreferences.setBool('isFirstAppOpen', false),
          }),
        }else{
          mp_globalData.sharedPreferences.setBool('isFirstAppOpen', false),
        }
      }
    );
  }

  void ToMain () {
    Navigator.pushAndRemoveUntil(_context, MaterialPageRoute(builder: (context)=>mp_mainPage()), (route) => false);
  }
  void ToLogin () {
    Navigator.pushAndRemoveUntil(_context, MaterialPageRoute(builder: (context)=>mp_loginPage()), (route) => false);
  }
}